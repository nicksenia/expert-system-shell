﻿namespace ExpertSystemShellWinApp.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShellWinApp.Entities;

    public partial class EditDomenForm : Form
    {
        public EditDomenForm()
        {
            InitializeComponent();
        }

        public Domen Domen { get; set; }
        public EFormEnum Type { get; set; }
        public EditDomenForm(int count, EFormEnum type)
        {
            InitializeComponent();
            Domen = new Domen($"Domen_{count}", new List<DomenVal>(), Guid.NewGuid(), count);
            Type = type;
            InitView();
        }

        public EditDomenForm(Domen domen, EFormEnum type)
        {
            InitializeComponent();
            Type = type;
            Domen = domen;
            InitView();
        }

        private void InitView()
        {
            if (Type == EFormEnum.Add)
            {
                Text = "Добавление домена";
                tbNameDomen.Text = Domen.Name;
                tbNameDomen.Focus();
            }
            else
            {
                Text = "Редактирование домена";
                tbNameDomen.Text = Domen.Name;
                FillValues();
                tbValue.Focus();
            }
        }

        private void FillValues()
        {
            lvValuesDomen.Items.Clear();

            foreach (var value in Domen.DomenValues.OrderBy(domen => domen.Order))
            {
                var item = new ListViewItem(new string[] { value.Order.ToString(), value.Value })
                {
                    Tag = value
                };

                lvValuesDomen.Items.Add(item);
            }
        }

        private void EditDomenForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //MessageBox.Show("Сохранить данные?", "Информация", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
        }

        private void btnAddDomen_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbNameDomen.Text) || Domen.DomenValues.Count == 0)
            {
                Hide();
                DialogResult = DialogResult.Cancel;
                if (string.IsNullOrWhiteSpace(tbNameDomen.Text))
                {
                    MessageBox.Show("Пустое имя домена!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else /* пустые значения */
                {
                    MessageBox.Show("Значения домена не должны быть пустыми!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                DialogResult = DialogResult.OK;
                Domen.Name = tbNameDomen.Text.Trim();
            }
            Close();
        }

        private void btnCancelDomen_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnAddValueDomen_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbValue.Text))
            {
                var value = tbValue.Text;
                if (!Domen.DomenValues.Any<DomenVal>(x => x.Value == value))
                {
                    var countValues = Domen.DomenValues.Count;
                    var domenValue = new DomenVal(tbValue.Text.Trim(), Guid.NewGuid(), countValues);
                    Domen.DomenValues.Add(domenValue);
                    FillValues();
                    tbValue.Text = string.Empty;
                }
            }

            tbValue.Focus();
        }

        private void btnEditValueDomen_Click(object sender, EventArgs e)
        {
            if (lvValuesDomen.SelectedItems.Count > 0)
            {
                if (!string.IsNullOrEmpty(tbValue.Text))
                {
                    var selectedValue = (DomenVal)lvValuesDomen.SelectedItems[0].Tag;
                    var newValue = new DomenVal(tbValue.Text, selectedValue.Uuid, selectedValue.Order);
                    Domen.DomenValues.Remove(selectedValue);
                    Domen.DomenValues.Add(newValue);
                    FillValues();
                }
            }

            tbValue.Focus();
        }

        private void btnDeleteValueDomen_Click(object sender, EventArgs e)
        {
            if (lvValuesDomen.SelectedItems.Count > 0)
            {
                var selectedValue = (DomenVal)lvValuesDomen.SelectedItems[0].Tag;
                Domen.DomenValues.Remove(selectedValue);
                lvValuesDomen.Items.Remove(lvValuesDomen.SelectedItems[0]);
                for (int i = 0; i < Domen.DomenValues.Count; i++)
                {
                    ListViewItem item = null;
                    for (int j = 0; j < lvValuesDomen.Items.Count; j++)
                    {
                        if (((DomenVal)lvValuesDomen.Items[j].Tag).Value == Domen.DomenValues[i].Value)
                        {
                            item = lvValuesDomen.Items[j];
                        }
                    }
                    Domen.DomenValues[i].Order = lvValuesDomen.Items.IndexOf(item);
                }
            }

            FillValues();
        }

        private void lvValuesDomen_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                Point p = new Point(e.X, e.Y);
                Point point2 = lvValuesDomen.PointToClient(p);
                ListViewItem itemAt = lvValuesDomen.GetItemAt(point2.X, point2.Y);
                if (itemAt != null)
                {
                    var domenValue = ((DomenVal)data.Tag);
                    var newNumber = ((DomenVal)itemAt.Tag).Order;
                    domenValue.Order = newNumber;
                    Domen.DomenValues.Remove(domenValue);

                    // Вставка на новое место
                    Domen.DomenValues.Insert(domenValue.Order, domenValue);

                    // Пересчёт порядка у всех
                    for (int i = 0; i < Domen.DomenValues.Count; i++)
                    {
                        if (Domen.DomenValues[i].Order != i)
                        {
                            Domen.DomenValues[i].Order = i;
                        }
                    }

                    FillValues();
                    lvValuesDomen.Items[domenValue.Order].Selected = true;
                }
            }
        }

        private void lvValuesDomen_DragEnter(object sender, DragEventArgs e)
        {
            Utils.DragEnter(sender, e);
        }

        private void lvValuesDomen_DragOver(object sender, DragEventArgs e)
        {
            Utils.DragOver(sender, e);
        }

        private void lvValuesDomen_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvValuesDomen.SelectedItems.Count > 0)
            {
                if (string.IsNullOrWhiteSpace(tbValue.Text))
                {
                    var selectedValue = (DomenVal)lvValuesDomen.SelectedItems[0].Tag;
                    tbValue.Text = selectedValue.Value;
                    tbValue.Focus();
                }
            }
        }

        private void lvValuesDomen_ItemDrag(object sender, ItemDragEventArgs e)
        {
            Utils.ItemDrag(this, sender, e);
        }
    }
}
