﻿namespace ExpertSystemShellWinApp.Forms
{
    using Entities;

    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Windows.Forms;

    public partial class GoalConsultForm : Form
    {
        public List<Variable> Variables { get; set; }
        public Variable Goal { get; set; }
        public GoalConsultForm(List<Variable> variables)
        {
            InitializeComponent();
            Variables = variables;
            Goal = new Variable();
            cmbGoalConsult.DataSource = Variables.OrderBy(x => x.Order).Where(x => x.VariableType != EVariableType.Requested).ToList();
            cmbGoalConsult.DisplayMember = "Name";
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cmbGoalConsult_SelectedIndexChanged(object sender, EventArgs e)
        {
            Goal = (Variable)cmbGoalConsult.SelectedItem;
        }
    }
}
