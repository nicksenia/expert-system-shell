﻿namespace ExpertSystemShellWinApp.Forms
{
    partial class EditDomenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbNameDome = new System.Windows.Forms.GroupBox();
            this.tbNameDomen = new System.Windows.Forms.TextBox();
            this.gbValuesDomen = new System.Windows.Forms.GroupBox();
            this.btnCancelDomen = new System.Windows.Forms.Button();
            this.btnAddDomen = new System.Windows.Forms.Button();
            this.gbValue = new System.Windows.Forms.GroupBox();
            this.btnDeleteValueDomen = new System.Windows.Forms.Button();
            this.btnEditValueDomen = new System.Windows.Forms.Button();
            this.btnAddValueDomen = new System.Windows.Forms.Button();
            this.tbValue = new System.Windows.Forms.TextBox();
            this.lvValuesDomen = new System.Windows.Forms.ListView();
            this.Number = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Value = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbNameDome.SuspendLayout();
            this.gbValuesDomen.SuspendLayout();
            this.gbValue.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbNameDome
            // 
            this.gbNameDome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbNameDome.Controls.Add(this.tbNameDomen);
            this.gbNameDome.Location = new System.Drawing.Point(11, 9);
            this.gbNameDome.Name = "gbNameDome";
            this.gbNameDome.Size = new System.Drawing.Size(474, 53);
            this.gbNameDome.TabIndex = 0;
            this.gbNameDome.TabStop = false;
            this.gbNameDome.Text = "Имя домена";
            // 
            // tbNameDomen
            // 
            this.tbNameDomen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNameDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbNameDomen.Location = new System.Drawing.Point(16, 19);
            this.tbNameDomen.Name = "tbNameDomen";
            this.tbNameDomen.Size = new System.Drawing.Size(441, 21);
            this.tbNameDomen.TabIndex = 0;
            // 
            // gbValuesDomen
            // 
            this.gbValuesDomen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbValuesDomen.Controls.Add(this.btnCancelDomen);
            this.gbValuesDomen.Controls.Add(this.btnAddDomen);
            this.gbValuesDomen.Controls.Add(this.gbValue);
            this.gbValuesDomen.Controls.Add(this.lvValuesDomen);
            this.gbValuesDomen.Location = new System.Drawing.Point(11, 68);
            this.gbValuesDomen.Name = "gbValuesDomen";
            this.gbValuesDomen.Size = new System.Drawing.Size(474, 323);
            this.gbValuesDomen.TabIndex = 1;
            this.gbValuesDomen.TabStop = false;
            this.gbValuesDomen.Text = "Список допустимых значений";
            // 
            // btnCancelDomen
            // 
            this.btnCancelDomen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCancelDomen.Location = new System.Drawing.Point(338, 48);
            this.btnCancelDomen.Name = "btnCancelDomen";
            this.btnCancelDomen.Size = new System.Drawing.Size(119, 23);
            this.btnCancelDomen.TabIndex = 3;
            this.btnCancelDomen.Text = "Отмена";
            this.btnCancelDomen.UseVisualStyleBackColor = true;
            this.btnCancelDomen.Click += new System.EventHandler(this.btnCancelDomen_Click);
            // 
            // btnAddDomen
            // 
            this.btnAddDomen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddDomen.Location = new System.Drawing.Point(338, 19);
            this.btnAddDomen.Name = "btnAddDomen";
            this.btnAddDomen.Size = new System.Drawing.Size(119, 23);
            this.btnAddDomen.TabIndex = 2;
            this.btnAddDomen.Text = "Добавить";
            this.btnAddDomen.UseVisualStyleBackColor = true;
            this.btnAddDomen.Click += new System.EventHandler(this.btnAddDomen_Click);
            // 
            // gbValue
            // 
            this.gbValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbValue.Controls.Add(this.btnDeleteValueDomen);
            this.gbValue.Controls.Add(this.btnEditValueDomen);
            this.gbValue.Controls.Add(this.btnAddValueDomen);
            this.gbValue.Controls.Add(this.tbValue);
            this.gbValue.Location = new System.Drawing.Point(16, 159);
            this.gbValue.Name = "gbValue";
            this.gbValue.Size = new System.Drawing.Size(441, 153);
            this.gbValue.TabIndex = 1;
            this.gbValue.TabStop = false;
            this.gbValue.Text = "Допусимое значение";
            // 
            // btnDeleteValueDomen
            // 
            this.btnDeleteValueDomen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteValueDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDeleteValueDomen.Location = new System.Drawing.Point(15, 116);
            this.btnDeleteValueDomen.Name = "btnDeleteValueDomen";
            this.btnDeleteValueDomen.Size = new System.Drawing.Size(405, 28);
            this.btnDeleteValueDomen.TabIndex = 3;
            this.btnDeleteValueDomen.Text = "Удалить";
            this.btnDeleteValueDomen.UseVisualStyleBackColor = true;
            this.btnDeleteValueDomen.Click += new System.EventHandler(this.btnDeleteValueDomen_Click);
            // 
            // btnEditValueDomen
            // 
            this.btnEditValueDomen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditValueDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEditValueDomen.Location = new System.Drawing.Point(15, 82);
            this.btnEditValueDomen.Name = "btnEditValueDomen";
            this.btnEditValueDomen.Size = new System.Drawing.Size(405, 28);
            this.btnEditValueDomen.TabIndex = 2;
            this.btnEditValueDomen.Text = "Редактировать";
            this.btnEditValueDomen.UseVisualStyleBackColor = true;
            this.btnEditValueDomen.Click += new System.EventHandler(this.btnEditValueDomen_Click);
            // 
            // btnAddValueDomen
            // 
            this.btnAddValueDomen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddValueDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddValueDomen.Location = new System.Drawing.Point(15, 46);
            this.btnAddValueDomen.Name = "btnAddValueDomen";
            this.btnAddValueDomen.Size = new System.Drawing.Size(405, 30);
            this.btnAddValueDomen.TabIndex = 1;
            this.btnAddValueDomen.Text = "Добавить";
            this.btnAddValueDomen.UseVisualStyleBackColor = true;
            this.btnAddValueDomen.Click += new System.EventHandler(this.btnAddValueDomen_Click);
            // 
            // tbValue
            // 
            this.tbValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbValue.Location = new System.Drawing.Point(15, 19);
            this.tbValue.Name = "tbValue";
            this.tbValue.Size = new System.Drawing.Size(405, 21);
            this.tbValue.TabIndex = 0;
            // 
            // lvValuesDomen
            // 
            this.lvValuesDomen.AllowDrop = true;
            this.lvValuesDomen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvValuesDomen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Number,
            this.Value});
            this.lvValuesDomen.FullRowSelect = true;
            this.lvValuesDomen.GridLines = true;
            this.lvValuesDomen.Location = new System.Drawing.Point(16, 19);
            this.lvValuesDomen.Name = "lvValuesDomen";
            this.lvValuesDomen.Size = new System.Drawing.Size(316, 134);
            this.lvValuesDomen.TabIndex = 0;
            this.lvValuesDomen.UseCompatibleStateImageBehavior = false;
            this.lvValuesDomen.View = System.Windows.Forms.View.Details;
            this.lvValuesDomen.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvValuesDomen_ItemDrag);
            this.lvValuesDomen.SelectedIndexChanged += new System.EventHandler(this.lvValuesDomen_SelectedIndexChanged);
            this.lvValuesDomen.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvValuesDomen_DragDrop);
            this.lvValuesDomen.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvValuesDomen_DragEnter);
            this.lvValuesDomen.DragOver += new System.Windows.Forms.DragEventHandler(this.lvValuesDomen_DragOver);
            // 
            // Number
            // 
            this.Number.Text = "№";
            this.Number.Width = 40;
            // 
            // Value
            // 
            this.Value.Text = "Значение";
            this.Value.Width = 262;
            // 
            // EditDomenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 401);
            this.Controls.Add(this.gbValuesDomen);
            this.Controls.Add(this.gbNameDome);
            this.Name = "EditDomenForm";
            this.Text = "Редактирование домена";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditDomenForm_FormClosing);
            this.gbNameDome.ResumeLayout(false);
            this.gbNameDome.PerformLayout();
            this.gbValuesDomen.ResumeLayout(false);
            this.gbValue.ResumeLayout(false);
            this.gbValue.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbNameDome;
        private System.Windows.Forms.TextBox tbNameDomen;
        private System.Windows.Forms.GroupBox gbValuesDomen;
        private System.Windows.Forms.Button btnCancelDomen;
        private System.Windows.Forms.Button btnAddDomen;
        private System.Windows.Forms.GroupBox gbValue;
        private System.Windows.Forms.Button btnDeleteValueDomen;
        private System.Windows.Forms.Button btnEditValueDomen;
        private System.Windows.Forms.Button btnAddValueDomen;
        private System.Windows.Forms.TextBox tbValue;
        private System.Windows.Forms.ListView lvValuesDomen;
        private System.Windows.Forms.ColumnHeader Value;
        private System.Windows.Forms.ColumnHeader Number;
    }
}