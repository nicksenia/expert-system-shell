﻿namespace ExpertSystemShellWinApp.Forms
{
    using ExpertSystemShellWinApp.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    public partial class EditFactForm : Form
    {
        public Fact Fact { get; set; }
        public List<Variable> VariableSet { get; set; }
        public List<Domen> DomenSet { get; set; }
        public List<Rule> RuleSet { get; set; }
        public EFormEnum Type { get; set; }
        public bool IsConsequences { get; set; }
        public EditFactForm(int count, List<Variable> variableSet, List<Domen> domenSet, List<Rule> ruleSet, EFormEnum type, bool isConsequence)
        {
            InitializeComponent();
            IsConsequences = isConsequence;
            VariableSet = variableSet;
            DomenSet = domenSet;
            RuleSet = ruleSet;
            Type = type;

            Fact = new Fact
            {
                Order = count,
                Variable = null,
                Value = null
            };

            InitView();
        }

        public EditFactForm(Fact fact, List<Variable> variableSet, List<Domen> domenSet, List<Rule> ruleSet, EFormEnum type, bool isConsequence)
        {
            InitializeComponent();
            IsConsequences = isConsequence;

            Fact = fact;
            VariableSet = variableSet;
            DomenSet = domenSet;
            RuleSet = ruleSet;
            Type = type;

            InitView();
        }

        private void InitView()
        {
            FillComboBoxes();
            if (Type == EFormEnum.Add)
            {
                Text = "Добавление факта";
                cmbVariable.Focus();
            }
            else
            {
                Text = "Редактирование факта";
                cmbVariable.SelectedItem = VariableSet.First(x => x.Name == Fact.Variable.Name);
                cmbValueVariable.SelectedItem = Fact.Value;
                cmbValueVariable.Focus();
            }
        }

        private void FillComboBoxes()
        {
            if (IsConsequences)
            {
                cmbVariable.DataSource = VariableSet.Where(x => x.VariableType != EVariableType.Requested).ToList();
            }
            else
            {
                cmbVariable.DataSource = VariableSet.ToList();
            }
            cmbVariable.DisplayMember = "Name";
        }

        private void EditFactForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //MessageBox.Show("Сохранить данные?", "Информация", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
        }

        private void btnAddFact_Click(object sender, EventArgs e)
        {
            Hide();
            if (cmbVariable.SelectedItem != null && cmbValueVariable.SelectedItem != null)
            {
                Fact.Variable = cmbVariable.SelectedItem as Variable;
                Fact.Value = cmbValueVariable.SelectedItem as DomenVal;
                DialogResult = DialogResult.OK;
            }
            else
            {
                if (cmbValueVariable.SelectedItem == null)
                {
                    MessageBox.Show("Не указана переменная!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Не указана значение!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                DialogResult = DialogResult.Cancel;
            }

            Close();
        }

        private void btnCancelFact_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void cmbVariable_SelectedIndexChanged(object sender, EventArgs e)
        {
            var currentVar = cmbVariable.SelectedItem as Variable;
            cmbValueVariable.DataSource = VariableSet.Where(x => x.Name == currentVar.Name).FirstOrDefault().Domen.DomenValues;
            cmbValueVariable.DisplayMember = "Value";

            if (cmbValueVariable.Items.Count > 0)
            {
                cmbValueVariable.SelectedItem = cmbValueVariable.Items[0];
            }
        }

        private void btnAddVariable_Click(object sender, EventArgs e)
        {
            var addEditForm = new EditVariableForm(VariableSet.Count(), DomenSet, RuleSet, EFormEnum.Add);
            var dialog = addEditForm.ShowDialog();

            if (dialog == DialogResult.OK)
            {
                var newVariable = addEditForm.Variable;
                /* добавили а такое имя уже существует */
                if (VariableSet.Any(x => x.Name == newVariable.Name))
                {
                    Utils.ShowError("Переменная");
                }
                else
                {
                    /* просто вставка в конец */
                    VariableSet.Add(addEditForm.Variable);
                    /* плюс обновляю комбики */
                    FillComboBoxes();
                }

                cmbVariable.SelectedItem = VariableSet[VariableSet.Count - 1];
            }
            addEditForm.Dispose();

        }
    }
}
