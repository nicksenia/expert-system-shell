﻿namespace ExpertSystemShellWinApp.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShellWinApp.Entities;

    public partial class VariableForm : Form
    {
        public List<Rule> RuleSet { get; }
        public List<Variable> VariableSet { get; set; }
        public List<Domen> DomenSet { get; set; }
        public VariableForm(List<Variable> variableSet, List<Domen> domenSet, List<Rule> ruleSet)
        {
            InitializeComponent();
            VariableSet = variableSet;
            DomenSet = domenSet;
            RuleSet = ruleSet;
            InitView();
        }

        private void InitView()
        {
            FillVariables();
        }

        private void FillVariables()
        {
            lvVariable.Items.Clear();
            foreach (var variable in VariableSet.OrderBy(x => x.Order))
            {
                var item = new ListViewItem(new string[]
                    { variable.Order.ToString(), variable.Name,
                        variable.Domen.Name, Utils.DictVarialbeType[variable.VariableType] })
                {
                    Tag = variable
                };
                lvVariable.Items.Add(item);
            }
        }

        private void btnAddVariable_Click(object sender, EventArgs e)
        {
            var editVariableForm = new EditVariableForm(VariableSet.Count(), DomenSet, RuleSet, EFormEnum.Add);
            var editDialog = editVariableForm.ShowDialog();

            if (editDialog == DialogResult.OK)
            {
                var newVariable = editVariableForm.Variable;
                if (VariableSet.Any<Variable>(x => x.Name == newVariable.Name))
                {
                    Utils.ShowError("Переменная");
                }
                else
                {
                    if (lvVariable.SelectedItems.Count > 0)
                    {
                        var number = ((Variable)lvVariable.SelectedItems[0].Tag).Order + 1;
                        VariableSet.Insert(number, newVariable);
                        //пересчитываю порядок у всех
                        for (int i = 0; i < VariableSet.Count; i++)
                        {
                            if (VariableSet[i].Order != i)
                            {
                                VariableSet[i].Order = i;
                            }
                        }
                    }
                    else
                    {
                        VariableSet.Add(editVariableForm.Variable);
                    }
                    FillVariables();
                }
            }
            editVariableForm.Dispose();
        }

        private void btnEditVariable_Click(object sender, EventArgs e)
        {
            if (lvVariable.SelectedItems.Count > 0)
            {
                var oldVariable = (Variable)lvVariable.SelectedItems[0].Tag;
                var selectedVariable = new Variable(oldVariable);
                var oldName = selectedVariable.Name;

                var editVariableForm = new EditVariableForm(selectedVariable, DomenSet, RuleSet, EFormEnum.Edit);
                var editDialog = editVariableForm.ShowDialog();

                if (editDialog == DialogResult.OK)
                {
                    var newVariable = editVariableForm.Variable;
                    if (VariableSet.Any(x => x.Name == newVariable.Name && oldName != newVariable.Name))
                    {
                        Utils.ShowError("Переменная");
                    }
                    else
                    {
                        var usedInRules =
                            RuleSet.Any(x => x.Consequences.Any(y => y.Variable.Uuid == oldVariable.Uuid ||
                                        x.Premises.Any(z => z.Variable.Uuid == oldVariable.Uuid)));
                        if (usedInRules && oldVariable.Domen.Uuid != newVariable.Domen.Uuid)
                        {
                            var resultAnswer = MessageBox.Show("Переменная, используемая в правилах была изменена. Создать новую?"
                        , "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                            if (resultAnswer == DialogResult.OK)
                            {
                                /* если да меняем uuid и ставим на некст строчку с именем *_new */
                                newVariable.Uuid = Guid.NewGuid();
                                newVariable.Name += "_new";
                                /* увеличиваю порядок для грамотного отображения */
                                newVariable.Order += 1;
                                VariableSet.Insert(newVariable.Order, newVariable);
                                /* пересчет номеров */
                                for (int i = 0; i < VariableSet.Count; i++)
                                {
                                    if (VariableSet[i].Order != i)
                                    {
                                        VariableSet[i].Order = i;
                                    }
                                }
                            }
                        }
                        else
                        {
                            VariableSet.Remove(oldVariable);
                            VariableSet.Insert(newVariable.Order, newVariable);
                        }
                        FillVariables();
                    }
                }
                editVariableForm.Dispose();
            }
        }

        private void btnDeleteVariable_Click(object sender, EventArgs e)
        {
            if (lvVariable.SelectedItems.Count > 0)
            {
                var selectedVariable = (Variable)lvVariable.SelectedItems[0].Tag;
                var isRemove = true;
                /* спросить о том, что этот переменная юзается */
                var usedInRule = RuleSet.Any(x => x.Premises.Any(y => y.Variable.Uuid == selectedVariable.Uuid))
                    || RuleSet.Any(x => x.Consequences.Any(y => y.Variable.Uuid == selectedVariable.Uuid));

                if (usedInRule)
                {
                    var resultAnswer = MessageBox.Show("Эта переменная используется в правилах! При удалении будут удалены все правила, где она используется. Продолжить?"
                        , "Внимание", MessageBoxButtons.OKCancel);
                    if (resultAnswer == DialogResult.Cancel)
                    {
                        isRemove = false;
                    }
                }
                if (isRemove)
                {
                    VariableSet.Remove(selectedVariable);
                    lvVariable.Items.Remove(lvVariable.SelectedItems[0]);
                    for (int i = 0; i < VariableSet.Count; i++)
                    {
                        ListViewItem item = null;
                        for (int j = 0; j < lvVariable.Items.Count; j++)
                        {
                            if (((Variable)lvVariable.Items[j].Tag).Name == VariableSet[i].Name)
                            {
                                item = lvVariable.Items[j];
                            }
                        }
                        VariableSet[i].Order = lvVariable.Items.IndexOf(item);
                    }
                    FillVariables();
                }
            }
        }

        private void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void lvVariable_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                Point p = new Point(e.X, e.Y);
                Point point2 = lvVariable.PointToClient(p);
                ListViewItem itemAt = lvVariable.GetItemAt(point2.X, point2.Y);
                if (itemAt != null)
                {
                    var variable = ((Variable)data.Tag);
                    var newNumber = ((Variable)itemAt.Tag).Order;
                    variable.Order = newNumber;
                    VariableSet.Remove(variable);

                    // Вставка на новое место
                    VariableSet.Insert(variable.Order, variable);

                    // Пересчёт порядка у всех
                    for (int i = 0; i < VariableSet.Count; i++)
                    {
                        if (VariableSet[i].Order != i)
                        {
                            VariableSet[i].Order = i;
                        }
                    }

                    FillVariables();
                    lvVariable.Items[variable.Order].Selected = true;
                }
            }
        }

        private void lvVariable_DragEnter(object sender, DragEventArgs e)
        {
            Utils.DragEnter(sender, e);
        }

        private void lvVariable_DragOver(object sender, DragEventArgs e)
        {
            Utils.DragOver(sender, e);
        }

        private void lvVariable_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvVariable.SelectedItems.Count > 0)
            {
                tbQuestionText.Text = ((Variable)lvVariable.SelectedItems[0].Tag).Question;
            }
        }

        private void lvVariable_DoubleClick(object sender, EventArgs e)
        {
            btnEditVariable_Click(sender, e);
        }

        private void lvVariable_ItemDrag(object sender, ItemDragEventArgs e)
        {
            Utils.ItemDrag(this, sender, e);
        }
    }
}
