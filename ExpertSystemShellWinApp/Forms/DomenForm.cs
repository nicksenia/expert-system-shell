﻿namespace ExpertSystemShellWinApp.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShellWinApp.Entities;

    public partial class DomenForm : Form
    {
        public List<Rule> RuleSet { get; }
        public List<Domen> DomenSet { get; set; }
        public List<Variable> VariableSet { get; private set; }
        public DomenForm()
        {
            InitializeComponent();
        }

        public DomenForm(List<Domen> domenSet, List<Variable> variableSet, List<Rule> ruleSet)
        {
            InitializeComponent();
            DomenSet = domenSet;
            VariableSet = variableSet;
            RuleSet = ruleSet;
            InitView();
        }

        private void InitView()
        {
            FillDomens();
        }

        private void FillDomens()
        {
            lvDomen.Items.Clear();
            foreach (var domen in DomenSet.OrderBy(x => x.Order))
            {
                var item = new ListViewItem(new string[] { domen.Order.ToString(), domen.Name })
                {
                    Tag = domen
                };

                lvDomen.Items.Add(item);
            }
        }

        private void FillVariables(Domen currentDomen)
        {
            lvCurrentDomen.Items.Clear();
            if (currentDomen != null)
            {
                foreach (var value in currentDomen.DomenValues.OrderBy(x => x.Order))
                {
                    var item = new ListViewItem(new string[] { value.Order.ToString(), value.Value })
                    {
                        Tag = value
                    };
                    lvCurrentDomen.Items.Add(item);
                }
            }
        }

        private void btnAddDomen_Click(object sender, EventArgs e)
        {
            var editDomenForm = new EditDomenForm(DomenSet.Count, EFormEnum.Add);
            var editDialog = editDomenForm.ShowDialog();
            if (editDialog == DialogResult.OK)
            {
                var newDomen = editDomenForm.Domen;
                if (DomenSet.Any<Domen>(x => x.Name == newDomen.Name))
                {
                    Utils.ShowError("Домен");
                }
                else
                {
                    if (lvDomen.SelectedItems.Count > 0)
                    {
                        var number = ((Domen)lvDomen.SelectedItems[0].Tag).Order + 1;
                        DomenSet.Insert(number, newDomen);
                        //пересчитываю порядок у всех
                        for (int i = 0; i < DomenSet.Count; i++)
                        {
                            if (DomenSet[i].Order != i)
                            {
                                DomenSet[i].Order = i;
                            }
                        }
                    }
                    else
                    {
                        DomenSet.Add(newDomen);
                    }
                    FillDomens();
                    FillVariables(newDomen);
                }
            }

            editDomenForm.Dispose();
        }

        private void btnEditDomen_Click(object sender, EventArgs e)
        {
            if (lvDomen.SelectedItems.Count > 0)
            {
                var oldDomen = (Domen)lvDomen.SelectedItems[0].Tag;
                var selectedDomen = new Domen(oldDomen);
                var editDomenForm = new EditDomenForm(selectedDomen, EFormEnum.Edit);
                var editDialog = editDomenForm.ShowDialog();
                if (editDialog == DialogResult.OK)
                {
                    var newDomen = editDomenForm.Domen;
                    if (DomenSet.Any(x => x.Name == newDomen.Name && x.Uuid != newDomen.Uuid))
                    {
                        Utils.ShowError("Домен");
                    }
                    else
                    {
                        bool isRemovedUsedValues = false;
                        foreach (var rule in RuleSet)
                        {
                            foreach (var fact in rule.Premises)
                            {
                                /* если среди значений домена нету использующегося в посылке, сравнение по имени */
                                if (fact.Variable.Domen.Uuid == newDomen.Uuid && !newDomen.DomenValues.Any(x => x.Value == fact.Value.Value))
                                {
                                    isRemovedUsedValues = true;
                                    break;
                                }
                            }
                            foreach (var fact in rule.Consequences)
                            {
                                /* если среди значений домена нету использующегося в заключении, сравнение по имени */
                                if (fact.Variable.Domen.Uuid == newDomen.Uuid && !newDomen.DomenValues.Any(x => x.Value == fact.Value.Value))
                                {
                                    isRemovedUsedValues = true;
                                    break;
                                }
                            }
                        }

                        if (isRemovedUsedValues)
                        {
                            /* спрашиваем о том, что вы удалили использующиеся значения, добавить ли новый */
                            var resultAnswer = MessageBox.Show("Были удалены использующиеся значения в домене! Создать новый домен?", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                            if (resultAnswer == DialogResult.OK)
                            {
                                /* если да меняем uuid и ставим на некст строчку с именем *_new */
                                newDomen.Name += "_new";
                                newDomen.Uuid = Guid.NewGuid();
                                newDomen.Order += 1;
                                DomenSet.Insert(newDomen.Order, newDomen);
                                /* пересчитываем порядок */
                                for (int i = 0; i < DomenSet.Count; i++)
                                {
                                    if (DomenSet[i].Order != i)
                                    {
                                        DomenSet[i].Order = i;
                                    }
                                }
                                FillDomens();
                                FillVariables(newDomen);
                            }
                        }
                        else
                        {
                            DomenSet.Remove(oldDomen);
                            DomenSet.Insert(newDomen.Order, newDomen);
                            FillDomens();
                            FillVariables(newDomen);

                        }
                    }
                }

                editDomenForm.Dispose();
            }
        }

        private void btnDeleteDomen_Click(object sender, EventArgs e)
        {
            if (lvDomen.SelectedItems.Count > 0)
            {
                var selectedDomen = (Domen)lvDomen.SelectedItems[0].Tag;
                var isRemove = true;
                var usedInRule = RuleSet.Any(x => x.Premises.Any(y => y.Variable.Domen.Name == selectedDomen.Name))
                    || RuleSet.Any(x => x.Consequences.Any(y => y.Variable.Domen.Name == selectedDomen.Name));
                var usedInVariable = VariableSet.Any(x => x.Domen.Uuid == selectedDomen.Uuid);

                if (usedInRule)
                {
                    var resultAnswer = MessageBox.Show("Этот домен используется в правилах! При удалении будут удалены все правила, где он используется. Продолжить?", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (resultAnswer == DialogResult.Cancel)
                    {
                        isRemove = false;
                    }
                }
                if (isRemove)
                {
                    DomenSet.Remove(selectedDomen);
                    VariableSet.RemoveAll(x => x.Domen.Uuid == selectedDomen.Uuid);
                    lvDomen.Items.Remove(lvDomen.SelectedItems[0]);
                    for (int i = 0; i < DomenSet.Count; i++)
                    {
                        ListViewItem item = null;
                        for (int j = 0; j < lvDomen.Items.Count; j++)
                        {
                            if (((Domen)lvDomen.Items[j].Tag).Name == DomenSet[i].Name)
                            {
                                item = lvDomen.Items[j];
                            }
                        }
                        DomenSet[i].Order = lvDomen.Items.IndexOf(item);
                    }
                    FillDomens();
                    lvCurrentDomen.Items.Clear();
                }
            }
        }

        private void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void lvDomen_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                Point p = new Point(e.X, e.Y);
                Point point2 = lvDomen.PointToClient(p);
                ListViewItem itemAt = lvDomen.GetItemAt(point2.X, point2.Y);
                if (itemAt != null)
                {
                    var domen = ((Domen)data.Tag);
                    var newNumber = ((Domen)itemAt.Tag).Order;
                    domen.Order = newNumber;
                    DomenSet.Remove(domen);

                    // Вставка на новое место
                    DomenSet.Insert(domen.Order, domen);

                    // Пересчёт порядка у всех
                    for (int i = 0; i < DomenSet.Count; i++)
                    {
                        if (DomenSet[i].Order != i)
                        {
                            DomenSet[i].Order = i;
                        }
                    }

                    FillDomens();
                    FillVariables(((Domen)data.Tag));
                    lvDomen.Items[domen.Order].Selected = true;
                }
            }
        }

        private void lvDomen_DragEnter(object sender, DragEventArgs e)
        {
            Utils.DragEnter(sender, e);
        }

        private void lvDomen_DragOver(object sender, DragEventArgs e)
        {
            Utils.DragOver(sender, e);
        }

        private void lvCurrentDomen_DragDrop(object sender, DragEventArgs e)
        {
            if (lvDomen.SelectedItems.Count != 0)
            {
                var domen = (Domen)lvDomen.SelectedItems[0].Tag;
                if (e.Data.GetDataPresent(typeof(ListViewItem)))
                {
                    ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                    Point p = new Point(e.X, e.Y);
                    Point point2 = lvCurrentDomen.PointToClient(p);
                    ListViewItem itemAt = lvCurrentDomen.GetItemAt(point2.X, point2.Y);
                    if (itemAt != null)
                    {
                        //беру выбранное значение
                        var domenValue = ((DomenVal)data.Tag);
                        //беру его новый номер
                        var newNumber = ((DomenVal)itemAt.Tag).Order;
                        domenValue.Order = newNumber;
                        //удаляю выбранное значение
                        domen.DomenValues.Remove(domenValue);
                        //вставляю его на новое место = новый номер
                        domen.DomenValues.Insert(domenValue.Order, domenValue);
                        //пересчитываю порядок у всех
                        for (int i = 0; i < domen.DomenValues.Count; i++)
                        {
                            if (domen.DomenValues[i].Order != i)
                            {
                                domen.DomenValues[i].Order = i;
                            }
                        }

                        FillVariables(domen);
                        lvCurrentDomen.Items[domenValue.Order].Selected = true;
                    }
                }
            }
        }

        private void lvCurrentDomen_DragEnter(object sender, DragEventArgs e)
        {
            Utils.DragEnter(sender, e);
        }

        private void lvCurrentDomen_DragOver(object sender, DragEventArgs e)
        {
            Utils.DragOver(sender, e);
        }

        private void lvDomen_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvDomen.SelectedItems.Count > 0)
            {
                var selectedDomain = (Domen)lvDomen.SelectedItems[0].Tag;
                FillVariables(selectedDomain);
            }
        }

        private void lvDomen_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnEditDomen_Click(sender, e);
        }

        private void lvDomen_ItemDrag(object sender, ItemDragEventArgs e)
        {
            Utils.ItemDrag(this, sender, e);
        }

        private void lvCurrentDomen_ItemDrag(object sender, ItemDragEventArgs e)
        {
            Utils.ItemDrag(this, sender, e);
        }
    }
}