﻿namespace ExpertSystemShellWinApp.Forms
{
    partial class EditRuleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbNameRule = new System.Windows.Forms.TextBox();
            this.btnAddPremises = new System.Windows.Forms.Button();
            this.btnAddConsequences = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbRuleConditions = new System.Windows.Forms.GroupBox();
            this.btnDeletePremises = new System.Windows.Forms.Button();
            this.btnEditPremises = new System.Windows.Forms.Button();
            this.lvPremises = new System.Windows.Forms.ListView();
            this.colNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colFact = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbRuleConclusions = new System.Windows.Forms.GroupBox();
            this.btnDeleteConsequences = new System.Windows.Forms.Button();
            this.btnEditConsequences = new System.Windows.Forms.Button();
            this.lvConsequences = new System.Windows.Forms.ListView();
            this.colNumbCons = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colValCons = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbReason = new System.Windows.Forms.GroupBox();
            this.tbReason = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.gbRuleConditions.SuspendLayout();
            this.gbRuleConclusions.SuspendLayout();
            this.gbReason.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbNameRule
            // 
            this.tbNameRule.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNameRule.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNameRule.Location = new System.Drawing.Point(6, 20);
            this.tbNameRule.Name = "tbNameRule";
            this.tbNameRule.Size = new System.Drawing.Size(590, 21);
            this.tbNameRule.TabIndex = 1;
            // 
            // btnAddPremises
            // 
            this.btnAddPremises.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnAddPremises.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddPremises.Location = new System.Drawing.Point(441, 19);
            this.btnAddPremises.Name = "btnAddPremises";
            this.btnAddPremises.Size = new System.Drawing.Size(155, 26);
            this.btnAddPremises.TabIndex = 12;
            this.btnAddPremises.Text = "Добавить";
            this.btnAddPremises.UseVisualStyleBackColor = true;
            this.btnAddPremises.Click += new System.EventHandler(this.btnAddPremises_Click);
            // 
            // btnAddConsequences
            // 
            this.btnAddConsequences.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnAddConsequences.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddConsequences.Location = new System.Drawing.Point(441, 27);
            this.btnAddConsequences.Name = "btnAddConsequences";
            this.btnAddConsequences.Size = new System.Drawing.Size(154, 27);
            this.btnAddConsequences.TabIndex = 20;
            this.btnAddConsequences.Text = "Добавить";
            this.btnAddConsequences.UseVisualStyleBackColor = true;
            this.btnAddConsequences.Click += new System.EventHandler(this.btnAddConsequences_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tbNameRule);
            this.groupBox1.Location = new System.Drawing.Point(6, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(613, 56);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Имя правила";
            // 
            // gbRuleConditions
            // 
            this.gbRuleConditions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbRuleConditions.Controls.Add(this.btnDeletePremises);
            this.gbRuleConditions.Controls.Add(this.btnEditPremises);
            this.gbRuleConditions.Controls.Add(this.lvPremises);
            this.gbRuleConditions.Controls.Add(this.btnAddPremises);
            this.gbRuleConditions.Location = new System.Drawing.Point(6, 76);
            this.gbRuleConditions.Name = "gbRuleConditions";
            this.gbRuleConditions.Size = new System.Drawing.Size(613, 178);
            this.gbRuleConditions.TabIndex = 25;
            this.gbRuleConditions.TabStop = false;
            this.gbRuleConditions.Text = "Условия правила";
            // 
            // btnDeletePremises
            // 
            this.btnDeletePremises.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnDeletePremises.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeletePremises.Location = new System.Drawing.Point(441, 83);
            this.btnDeletePremises.Name = "btnDeletePremises";
            this.btnDeletePremises.Size = new System.Drawing.Size(155, 26);
            this.btnDeletePremises.TabIndex = 15;
            this.btnDeletePremises.Text = "Удалить";
            this.btnDeletePremises.UseVisualStyleBackColor = true;
            this.btnDeletePremises.Click += new System.EventHandler(this.btnDeletePremises_Click);
            // 
            // btnEditPremises
            // 
            this.btnEditPremises.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnEditPremises.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditPremises.Location = new System.Drawing.Point(441, 51);
            this.btnEditPremises.Name = "btnEditPremises";
            this.btnEditPremises.Size = new System.Drawing.Size(154, 26);
            this.btnEditPremises.TabIndex = 14;
            this.btnEditPremises.Text = "Редактировать";
            this.btnEditPremises.UseVisualStyleBackColor = true;
            this.btnEditPremises.Click += new System.EventHandler(this.btnEditPremises_Click);
            // 
            // lvPremises
            // 
            this.lvPremises.AllowDrop = true;
            this.lvPremises.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvPremises.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNumber,
            this.colFact});
            this.lvPremises.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvPremises.FullRowSelect = true;
            this.lvPremises.GridLines = true;
            this.lvPremises.Location = new System.Drawing.Point(5, 20);
            this.lvPremises.Name = "lvPremises";
            this.lvPremises.Size = new System.Drawing.Size(417, 141);
            this.lvPremises.TabIndex = 13;
            this.lvPremises.UseCompatibleStateImageBehavior = false;
            this.lvPremises.View = System.Windows.Forms.View.Details;
            this.lvPremises.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvPremises_ItemDrag);
            this.lvPremises.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvPremises_DragDrop);
            this.lvPremises.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvPremises_DragEnter);
            this.lvPremises.DragOver += new System.Windows.Forms.DragEventHandler(this.lvPremises_DragOver);
            this.lvPremises.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvPremises_MouseDoubleClick);
            // 
            // colNumber
            // 
            this.colNumber.Text = "№";
            this.colNumber.Width = 48;
            // 
            // colFact
            // 
            this.colFact.Text = "Условие";
            this.colFact.Width = 187;
            // 
            // gbRuleConclusions
            // 
            this.gbRuleConclusions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbRuleConclusions.Controls.Add(this.btnDeleteConsequences);
            this.gbRuleConclusions.Controls.Add(this.btnEditConsequences);
            this.gbRuleConclusions.Controls.Add(this.lvConsequences);
            this.gbRuleConclusions.Controls.Add(this.btnAddConsequences);
            this.gbRuleConclusions.Location = new System.Drawing.Point(6, 260);
            this.gbRuleConclusions.Name = "gbRuleConclusions";
            this.gbRuleConclusions.Size = new System.Drawing.Size(613, 181);
            this.gbRuleConclusions.TabIndex = 26;
            this.gbRuleConclusions.TabStop = false;
            this.gbRuleConclusions.Text = "Заключения правила";
            // 
            // btnDeleteConsequences
            // 
            this.btnDeleteConsequences.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnDeleteConsequences.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteConsequences.Location = new System.Drawing.Point(441, 93);
            this.btnDeleteConsequences.Name = "btnDeleteConsequences";
            this.btnDeleteConsequences.Size = new System.Drawing.Size(155, 27);
            this.btnDeleteConsequences.TabIndex = 23;
            this.btnDeleteConsequences.Text = "Удалить";
            this.btnDeleteConsequences.UseVisualStyleBackColor = true;
            this.btnDeleteConsequences.Click += new System.EventHandler(this.btnDeleteConsequences_Click);
            // 
            // btnEditConsequences
            // 
            this.btnEditConsequences.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnEditConsequences.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditConsequences.Location = new System.Drawing.Point(441, 60);
            this.btnEditConsequences.Name = "btnEditConsequences";
            this.btnEditConsequences.Size = new System.Drawing.Size(155, 27);
            this.btnEditConsequences.TabIndex = 22;
            this.btnEditConsequences.Text = "Редактировать";
            this.btnEditConsequences.UseVisualStyleBackColor = true;
            this.btnEditConsequences.Click += new System.EventHandler(this.btnEditConsequences_Click);
            // 
            // lvConsequences
            // 
            this.lvConsequences.AllowDrop = true;
            this.lvConsequences.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvConsequences.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNumbCons,
            this.colValCons});
            this.lvConsequences.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvConsequences.FullRowSelect = true;
            this.lvConsequences.GridLines = true;
            this.lvConsequences.Location = new System.Drawing.Point(6, 26);
            this.lvConsequences.Name = "lvConsequences";
            this.lvConsequences.Size = new System.Drawing.Size(416, 136);
            this.lvConsequences.TabIndex = 21;
            this.lvConsequences.UseCompatibleStateImageBehavior = false;
            this.lvConsequences.View = System.Windows.Forms.View.Details;
            this.lvConsequences.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvConsequences_ItemDrag);
            this.lvConsequences.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvConsequences_DragDrop);
            this.lvConsequences.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvConsequences_DragEnter);
            this.lvConsequences.DragOver += new System.Windows.Forms.DragEventHandler(this.lvConsequences_DragOver);
            this.lvConsequences.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvConsequences_MouseDoubleClick);
            // 
            // colNumbCons
            // 
            this.colNumbCons.Text = "№";
            this.colNumbCons.Width = 44;
            // 
            // colValCons
            // 
            this.colValCons.Text = "Заключение";
            this.colValCons.Width = 189;
            // 
            // gbReason
            // 
            this.gbReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbReason.Controls.Add(this.tbReason);
            this.gbReason.Location = new System.Drawing.Point(7, 447);
            this.gbReason.Name = "gbReason";
            this.gbReason.Size = new System.Drawing.Size(612, 59);
            this.gbReason.TabIndex = 27;
            this.gbReason.TabStop = false;
            this.gbReason.Text = "Причина";
            // 
            // tbReason
            // 
            this.tbReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReason.Location = new System.Drawing.Point(20, 24);
            this.tbReason.Name = "tbReason";
            this.tbReason.Size = new System.Drawing.Size(575, 20);
            this.tbReason.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(393, 512);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(226, 23);
            this.btnCancel.TabIndex = 28;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.Location = new System.Drawing.Point(7, 512);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(226, 23);
            this.btnOk.TabIndex = 29;
            this.btnOk.Text = "ОК";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // EditRuleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 545);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.gbReason);
            this.Controls.Add(this.gbRuleConclusions);
            this.Controls.Add(this.gbRuleConditions);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "EditRuleForm";
            this.Text = "Редактирование правила";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditRuleForm_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbRuleConditions.ResumeLayout(false);
            this.gbRuleConclusions.ResumeLayout(false);
            this.gbReason.ResumeLayout(false);
            this.gbReason.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox tbNameRule;
        private System.Windows.Forms.Button btnAddPremises;
        private System.Windows.Forms.Button btnAddConsequences;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbRuleConditions;
        private System.Windows.Forms.ListView lvPremises;
        private System.Windows.Forms.ColumnHeader colFact;
        private System.Windows.Forms.Button btnDeletePremises;
        private System.Windows.Forms.Button btnEditPremises;
        private System.Windows.Forms.GroupBox gbRuleConclusions;
        private System.Windows.Forms.ListView lvConsequences;
        private System.Windows.Forms.Button btnDeleteConsequences;
        private System.Windows.Forms.Button btnEditConsequences;
        private System.Windows.Forms.ColumnHeader colNumbCons;
        private System.Windows.Forms.ColumnHeader colNumber;
        private System.Windows.Forms.GroupBox gbReason;
        private System.Windows.Forms.TextBox tbReason;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ColumnHeader colValCons;
    }
}