﻿namespace ExpertSystemShellWinApp.Forms
{
    partial class GoalConsultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbGoalConsult = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.gbGoalConsult = new System.Windows.Forms.GroupBox();
            this.gbGoalConsult.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbGoalConsult
            // 
            this.cmbGoalConsult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoalConsult.FormattingEnabled = true;
            this.cmbGoalConsult.Location = new System.Drawing.Point(6, 19);
            this.cmbGoalConsult.Name = "cmbGoalConsult";
            this.cmbGoalConsult.Size = new System.Drawing.Size(258, 21);
            this.cmbGoalConsult.TabIndex = 0;
            this.cmbGoalConsult.SelectedIndexChanged += new System.EventHandler(this.cmbGoalConsult_SelectedIndexChanged);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(10, 69);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(270, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "ОК";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // gbGoalConsult
            // 
            this.gbGoalConsult.Controls.Add(this.cmbGoalConsult);
            this.gbGoalConsult.Location = new System.Drawing.Point(10, 10);
            this.gbGoalConsult.Name = "gbGoalConsult";
            this.gbGoalConsult.Size = new System.Drawing.Size(270, 50);
            this.gbGoalConsult.TabIndex = 3;
            this.gbGoalConsult.TabStop = false;
            this.gbGoalConsult.Text = "Цель консультации";
            // 
            // GoalConsultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 104);
            this.Controls.Add(this.gbGoalConsult);
            this.Controls.Add(this.btnOK);
            this.MaximumSize = new System.Drawing.Size(308, 143);
            this.MinimumSize = new System.Drawing.Size(308, 143);
            this.Name = "GoalConsultForm";
            this.Text = "Цель консультации";
            this.gbGoalConsult.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbGoalConsult;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox gbGoalConsult;
    }
}