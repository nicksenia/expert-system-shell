﻿namespace ExpertSystemShellWinApp.Forms
{
    using Entities;

    using System;
    using System.Windows.Forms;

    public partial class ConsultForm : Form
    {
        public DomenVal Answer { get; set; }
        public ConsultForm(string consultGoal)
        {
            InitializeComponent();
            tbGoal.Text = consultGoal;
        }

        private void btnAnswer_Click(object sender, EventArgs e)
        {
            lbDialog.Items.Add($"Ваш ответ: {cmbAnswer.Text}");
            Answer = cmbAnswer.SelectedItem as DomenVal;
            DialogResult = DialogResult.OK;
        }

        private void btnStopConsult_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        public void AskQuestion(Variable variable)
        {
            cmbAnswer.DataSource = variable.Domen.DomenValues;
            cmbAnswer.DisplayMember = "Value";

            if (variable.VariableType == EVariableType.Requested)
            {
                lbDialog.Items.Add($"Вопрос: {variable.Question}");
            }

            if (variable.VariableType == EVariableType.DerivableRequested)
            {
                lbDialog.Items.Add($"Вопрос: {variable.Name}");
            }

            lbDialog.SelectedItem = lbDialog.Items[lbDialog.Items.Count - 1];
        }
    }
}
