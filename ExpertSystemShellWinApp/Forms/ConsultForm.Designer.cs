﻿namespace ExpertSystemShellWinApp.Forms
{
    partial class ConsultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnswer = new System.Windows.Forms.Button();
            this.cmbAnswer = new System.Windows.Forms.ComboBox();
            this.gbAnswer = new System.Windows.Forms.GroupBox();
            this.lbDialog = new System.Windows.Forms.ListBox();
            this.gbDialog = new System.Windows.Forms.GroupBox();
            this.gbGoal = new System.Windows.Forms.GroupBox();
            this.tbGoal = new System.Windows.Forms.TextBox();
            this.btnStopConsult = new System.Windows.Forms.Button();
            this.gbAnswer.SuspendLayout();
            this.gbDialog.SuspendLayout();
            this.gbGoal.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnswer
            // 
            this.btnAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAnswer.Location = new System.Drawing.Point(422, 21);
            this.btnAnswer.Name = "btnAnswer";
            this.btnAnswer.Size = new System.Drawing.Size(104, 23);
            this.btnAnswer.TabIndex = 0;
            this.btnAnswer.Text = "Ответить";
            this.btnAnswer.UseVisualStyleBackColor = true;
            this.btnAnswer.Click += new System.EventHandler(this.btnAnswer_Click);
            // 
            // cmbAnswer
            // 
            this.cmbAnswer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbAnswer.FormattingEnabled = true;
            this.cmbAnswer.Location = new System.Drawing.Point(19, 21);
            this.cmbAnswer.Name = "cmbAnswer";
            this.cmbAnswer.Size = new System.Drawing.Size(385, 23);
            this.cmbAnswer.TabIndex = 1;
            // 
            // gbAnswer
            // 
            this.gbAnswer.Controls.Add(this.cmbAnswer);
            this.gbAnswer.Controls.Add(this.btnAnswer);
            this.gbAnswer.Location = new System.Drawing.Point(12, 257);
            this.gbAnswer.Name = "gbAnswer";
            this.gbAnswer.Size = new System.Drawing.Size(542, 53);
            this.gbAnswer.TabIndex = 2;
            this.gbAnswer.TabStop = false;
            this.gbAnswer.Text = "Ответ";
            // 
            // lbDialog
            // 
            this.lbDialog.FormattingEnabled = true;
            this.lbDialog.HorizontalScrollbar = true;
            this.lbDialog.Location = new System.Drawing.Point(19, 21);
            this.lbDialog.Name = "lbDialog";
            this.lbDialog.Size = new System.Drawing.Size(507, 147);
            this.lbDialog.TabIndex = 3;
            // 
            // gbDialog
            // 
            this.gbDialog.BackColor = System.Drawing.SystemColors.Control;
            this.gbDialog.Controls.Add(this.lbDialog);
            this.gbDialog.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbDialog.Location = new System.Drawing.Point(12, 62);
            this.gbDialog.Name = "gbDialog";
            this.gbDialog.Size = new System.Drawing.Size(542, 181);
            this.gbDialog.TabIndex = 4;
            this.gbDialog.TabStop = false;
            this.gbDialog.Text = "Диалог";
            // 
            // gbGoal
            // 
            this.gbGoal.Controls.Add(this.tbGoal);
            this.gbGoal.Location = new System.Drawing.Point(12, 6);
            this.gbGoal.Name = "gbGoal";
            this.gbGoal.Size = new System.Drawing.Size(542, 50);
            this.gbGoal.TabIndex = 5;
            this.gbGoal.TabStop = false;
            this.gbGoal.Text = "Цель консультации";
            // 
            // tbGoal
            // 
            this.tbGoal.Location = new System.Drawing.Point(19, 19);
            this.tbGoal.Name = "tbGoal";
            this.tbGoal.ReadOnly = true;
            this.tbGoal.Size = new System.Drawing.Size(507, 20);
            this.tbGoal.TabIndex = 0;
            // 
            // btnStopConsult
            // 
            this.btnStopConsult.Location = new System.Drawing.Point(12, 316);
            this.btnStopConsult.Name = "btnStopConsult";
            this.btnStopConsult.Size = new System.Drawing.Size(542, 33);
            this.btnStopConsult.TabIndex = 7;
            this.btnStopConsult.Text = "Прекратить консультацию";
            this.btnStopConsult.UseVisualStyleBackColor = true;
            this.btnStopConsult.Click += new System.EventHandler(this.btnStopConsult_Click);
            // 
            // ConsultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(566, 361);
            this.Controls.Add(this.btnStopConsult);
            this.Controls.Add(this.gbGoal);
            this.Controls.Add(this.gbDialog);
            this.Controls.Add(this.gbAnswer);
            this.MaximumSize = new System.Drawing.Size(582, 400);
            this.MinimumSize = new System.Drawing.Size(582, 400);
            this.Name = "ConsultForm";
            this.Text = "Консультация";
            this.gbAnswer.ResumeLayout(false);
            this.gbDialog.ResumeLayout(false);
            this.gbGoal.ResumeLayout(false);
            this.gbGoal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAnswer;
        private System.Windows.Forms.ComboBox cmbAnswer;
        private System.Windows.Forms.GroupBox gbAnswer;
        private System.Windows.Forms.ListBox lbDialog;
        private System.Windows.Forms.GroupBox gbDialog;
        private System.Windows.Forms.GroupBox gbGoal;
        private System.Windows.Forms.TextBox tbGoal;
        private System.Windows.Forms.Button btnStopConsult;
    }
}