﻿namespace ExpertSystemShellWinApp.Forms
{
    using Entities;
    using Rule = Entities.Rule;

    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Windows.Forms;

    public partial class ExplainForm : Form
    {
        private Dictionary<Rule, int> UsedRules { get; set; }
        private Dictionary<string, DomenVal> WorkMemory { get; set; }
        public ExplainForm(Dictionary<Rule, int> rules, Dictionary<string, DomenVal> workMemory)
        {
            InitializeComponent();
            UsedRules = rules;
            WorkMemory = workMemory;
            InitView();
            btnExpandRules.Focus();
        }

        public void InitView()
        {
            lvWorkMemory.Items.Clear();
            foreach (var kvp in WorkMemory)
            {
                var item = new ListViewItem(new string[] { kvp.Key, kvp.Value.Value });
                lvWorkMemory.Items.Add(item);
            }

            lvWorkMemory.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            Fill(0, null, null);
        }

        private bool Fill(int currentLevel, TreeNode parent, Rule parentRule)
        {
            if (currentLevel == 0)
            {
                foreach (var kvp in UsedRules.Where(x => x.Value == 0))
                {
                    var goalNode = new TreeNode($"Цель = {kvp.Key.Consequences[0].Variable.Name}");
                    var node = new TreeNode($"{kvp.Key.Name}: {kvp.Key.ToString()}");
                    var reasonNode = new TreeNode($"Причина = {kvp.Key.Reason}");
                    var resultNode = new TreeNode($"{kvp.Key.Consequences[0].Variable.Name} = {kvp.Key.Consequences[0].Value.Value}");
                    tvWorkMemory.Nodes.Add(goalNode);
                    tvWorkMemory.Nodes.Add(node);
                    tvWorkMemory.Nodes.Add(reasonNode);
                    tvWorkMemory.Nodes.Add(resultNode);
                    if (Fill(currentLevel + 1, node, kvp.Key))
                    {
                        Fill(currentLevel + 2, node, kvp.Key);
                    }
                }
            }
            else
            {
                foreach (var kvp in UsedRules.Where(x => x.Value == currentLevel))
                {
                    bool isGood = false;
                    foreach (var rule in parentRule.Premises)
                    {
                        foreach (var ruleCons in kvp.Key.Consequences)
                        {
                            if (rule.ToString() == ruleCons.ToString())
                            {
                                isGood = true;
                            }
                        }
                    }

                    if (isGood)
                    {
                        var goalNode = new TreeNode($"Цель = {kvp.Key.Consequences[0].Variable.Name}");
                        var node = new TreeNode($"{kvp.Key.Name}: {kvp.Key.ToString()}");
                        var reasonNode = new TreeNode($"Причина = {kvp.Key.Reason}");
                        var resultNode = new TreeNode($"{kvp.Key.Consequences[0].Variable.Name} = {kvp.Key.Consequences[0].Value.Value}");
                        parent.Nodes.Add(goalNode);
                        parent.Nodes.Add(node);
                        parent.Nodes.Add(reasonNode);
                        parent.Nodes.Add(resultNode);
                        Fill(currentLevel + 1, node, kvp.Key);
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void btnExpandRules_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            tvWorkMemory.ExpandAll();
            btnCollapseRules.Focus();
        }

        private void btnCollapseRules_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            tvWorkMemory.CollapseAll();
            btnExpandRules.Focus();
        }
    }
}
