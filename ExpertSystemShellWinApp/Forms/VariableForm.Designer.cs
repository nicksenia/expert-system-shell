﻿namespace ExpertSystemShellWinApp.Forms
{
    partial class VariableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbVariable = new System.Windows.Forms.GroupBox();
            this.lvVariable = new System.Windows.Forms.ListView();
            this.variableNumbet = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.variableName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.variableDomen = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.variableType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbActions = new System.Windows.Forms.GroupBox();
            this.btnDeleteVariable = new System.Windows.Forms.Button();
            this.btnEditVariable = new System.Windows.Forms.Button();
            this.btnAddVariable = new System.Windows.Forms.Button();
            this.gbQuestionText = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSaveAndClose = new System.Windows.Forms.Button();
            this.tbQuestionText = new System.Windows.Forms.TextBox();
            this.gbVariable.SuspendLayout();
            this.gbActions.SuspendLayout();
            this.gbQuestionText.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbVariable
            // 
            this.gbVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbVariable.Controls.Add(this.lvVariable);
            this.gbVariable.Location = new System.Drawing.Point(4, 12);
            this.gbVariable.Name = "gbVariable";
            this.gbVariable.Size = new System.Drawing.Size(429, 410);
            this.gbVariable.TabIndex = 0;
            this.gbVariable.TabStop = false;
            this.gbVariable.Text = "Переменные";
            // 
            // lvVariable
            // 
            this.lvVariable.AllowDrop = true;
            this.lvVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvVariable.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.variableNumbet,
            this.variableName,
            this.variableDomen,
            this.variableType});
            this.lvVariable.FullRowSelect = true;
            this.lvVariable.GridLines = true;
            this.lvVariable.Location = new System.Drawing.Point(9, 19);
            this.lvVariable.Name = "lvVariable";
            this.lvVariable.Size = new System.Drawing.Size(414, 385);
            this.lvVariable.TabIndex = 0;
            this.lvVariable.UseCompatibleStateImageBehavior = false;
            this.lvVariable.View = System.Windows.Forms.View.Details;
            this.lvVariable.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvVariable_ItemDrag);
            this.lvVariable.SelectedIndexChanged += new System.EventHandler(this.lvVariable_SelectedIndexChanged);
            this.lvVariable.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvVariable_DragDrop);
            this.lvVariable.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvVariable_DragEnter);
            this.lvVariable.DragOver += new System.Windows.Forms.DragEventHandler(this.lvVariable_DragOver);
            this.lvVariable.DoubleClick += new System.EventHandler(this.lvVariable_DoubleClick);
            // 
            // variableNumbet
            // 
            this.variableNumbet.Text = "№";
            // 
            // variableName
            // 
            this.variableName.Text = "Название";
            this.variableName.Width = 92;
            // 
            // variableDomen
            // 
            this.variableDomen.Text = "Домен";
            this.variableDomen.Width = 108;
            // 
            // variableType
            // 
            this.variableType.Text = "Тип";
            this.variableType.Width = 145;
            // 
            // gbActions
            // 
            this.gbActions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbActions.Controls.Add(this.btnDeleteVariable);
            this.gbActions.Controls.Add(this.btnEditVariable);
            this.gbActions.Controls.Add(this.btnAddVariable);
            this.gbActions.Location = new System.Drawing.Point(446, 18);
            this.gbActions.Name = "gbActions";
            this.gbActions.Size = new System.Drawing.Size(225, 130);
            this.gbActions.TabIndex = 1;
            this.gbActions.TabStop = false;
            this.gbActions.Text = "Действия";
            // 
            // btnDeleteVariable
            // 
            this.btnDeleteVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDeleteVariable.Location = new System.Drawing.Point(18, 87);
            this.btnDeleteVariable.Name = "btnDeleteVariable";
            this.btnDeleteVariable.Size = new System.Drawing.Size(190, 28);
            this.btnDeleteVariable.TabIndex = 2;
            this.btnDeleteVariable.Text = "Удалить";
            this.btnDeleteVariable.UseVisualStyleBackColor = true;
            this.btnDeleteVariable.Click += new System.EventHandler(this.btnDeleteVariable_Click);
            // 
            // btnEditVariable
            // 
            this.btnEditVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEditVariable.Location = new System.Drawing.Point(18, 53);
            this.btnEditVariable.Name = "btnEditVariable";
            this.btnEditVariable.Size = new System.Drawing.Size(190, 28);
            this.btnEditVariable.TabIndex = 1;
            this.btnEditVariable.Text = "Редактировать";
            this.btnEditVariable.UseVisualStyleBackColor = true;
            this.btnEditVariable.Click += new System.EventHandler(this.btnEditVariable_Click);
            // 
            // btnAddVariable
            // 
            this.btnAddVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddVariable.Location = new System.Drawing.Point(18, 19);
            this.btnAddVariable.Name = "btnAddVariable";
            this.btnAddVariable.Size = new System.Drawing.Size(190, 28);
            this.btnAddVariable.TabIndex = 0;
            this.btnAddVariable.Text = "Добавить";
            this.btnAddVariable.UseVisualStyleBackColor = true;
            this.btnAddVariable.Click += new System.EventHandler(this.btnAddVariable_Click);
            // 
            // gbQuestionText
            // 
            this.gbQuestionText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gbQuestionText.Controls.Add(this.btnClose);
            this.gbQuestionText.Controls.Add(this.btnSaveAndClose);
            this.gbQuestionText.Controls.Add(this.tbQuestionText);
            this.gbQuestionText.Location = new System.Drawing.Point(446, 154);
            this.gbQuestionText.Name = "gbQuestionText";
            this.gbQuestionText.Size = new System.Drawing.Size(225, 268);
            this.gbQuestionText.TabIndex = 2;
            this.gbQuestionText.TabStop = false;
            this.gbQuestionText.Text = "Текст вопроса";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(18, 239);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(190, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Закрыть";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSaveAndClose
            // 
            this.btnSaveAndClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAndClose.Location = new System.Drawing.Point(18, 209);
            this.btnSaveAndClose.Name = "btnSaveAndClose";
            this.btnSaveAndClose.Size = new System.Drawing.Size(190, 24);
            this.btnSaveAndClose.TabIndex = 1;
            this.btnSaveAndClose.Text = "Сохранить и закрыть";
            this.btnSaveAndClose.UseVisualStyleBackColor = true;
            this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
            // 
            // tbQuestionText
            // 
            this.tbQuestionText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbQuestionText.Location = new System.Drawing.Point(18, 19);
            this.tbQuestionText.Multiline = true;
            this.tbQuestionText.Name = "tbQuestionText";
            this.tbQuestionText.ReadOnly = true;
            this.tbQuestionText.Size = new System.Drawing.Size(190, 184);
            this.tbQuestionText.TabIndex = 0;
            // 
            // VariableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 434);
            this.Controls.Add(this.gbQuestionText);
            this.Controls.Add(this.gbActions);
            this.Controls.Add(this.gbVariable);
            this.Name = "VariableForm";
            this.Text = "Переменные";
            this.gbVariable.ResumeLayout(false);
            this.gbActions.ResumeLayout(false);
            this.gbQuestionText.ResumeLayout(false);
            this.gbQuestionText.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbVariable;
        private System.Windows.Forms.ListView lvVariable;
        private System.Windows.Forms.ColumnHeader variableName;
        private System.Windows.Forms.ColumnHeader variableDomen;
        private System.Windows.Forms.ColumnHeader variableType;
        private System.Windows.Forms.GroupBox gbActions;
        private System.Windows.Forms.Button btnDeleteVariable;
        private System.Windows.Forms.Button btnEditVariable;
        private System.Windows.Forms.Button btnAddVariable;
        private System.Windows.Forms.ColumnHeader variableNumbet;
        private System.Windows.Forms.GroupBox gbQuestionText;
        private System.Windows.Forms.TextBox tbQuestionText;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSaveAndClose;
    }
}