﻿namespace ExpertSystemShellWinApp.Forms
{
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    public partial class MainForm : Form
    {
        public static ExpSysShell Instanse { get; set; }
        public MainForm()
        {
            InitializeComponent();
            Instanse = new ExpSysShell();
            Instanse.BaseRule.DomenSet = new List<Domen>();
            Instanse.BaseRule.VariableSet = new List<Variable>();
            Instanse.BaseRule.RuleSet = new List<Rule>();
            InitRules();
        }

        private void InitRules()
        {
            lvRules.Items.Clear();
            foreach (var rule in Instanse.BaseRule.RuleSet.OrderBy(x => x.Order))
            {
                var item = new ListViewItem(new string[] { rule.Name, rule.ToString() })
                {
                    Tag = rule
                };
                lvRules.Items.Add(item);
            }
        }

        private void SetValuesInListViews(Rule rule = null)
        {
            lvPremises.Items.Clear();
            lvConsequences.Items.Clear();
            if (rule != null)
            {
                foreach (var fact in rule.Premises.OrderBy(x => x.Order))
                {
                    var item = new ListViewItem(new string[] { fact.ToString() })
                    {
                        Tag = fact
                    };
                    lvPremises.Items.Add(item);
                }

                foreach (var fact in rule.Consequences.OrderBy(x => x.Order))
                {
                    var item = new ListViewItem(new string[] { fact.ToString() })
                    {
                        Tag = fact
                    };
                    lvConsequences.Items.Add(item);
                }
            }
        }

        private void btnAddRule_Click(object sender, System.EventArgs e)
        {
            var ruleForm = new EditRuleForm(Instanse.BaseRule.RuleSet.Count, Instanse.BaseRule.VariableSet, Instanse.BaseRule.DomenSet, Instanse.BaseRule.RuleSet, EFormEnum.Add);
            var ruleFormDialog = ruleForm.ShowDialog();

            if (ruleFormDialog == DialogResult.OK)
            {
                var newRule = ruleForm.Rule;
                if (!Instanse.BaseRule.RuleSet.Any(x => x.Name == newRule.Name.Trim()))
                {
                    if (lvRules.SelectedItems.Count > 0)
                    {
                        var number = ((Rule)lvRules.SelectedItems[0].Tag).Order + 1;
                        Instanse.BaseRule.RuleSet.Insert(number, newRule);
                        // Пересчитывается порядок у всех.
                        for (int i = 0; i < Instanse.BaseRule.RuleSet.Count; i++)
                        {
                            if (Instanse.BaseRule.RuleSet[i].Order != i)
                            {
                                Instanse.BaseRule.RuleSet[i].Order = i;
                            }
                        }
                    }
                    else
                    {
                        Instanse.BaseRule.RuleSet.Add(newRule);
                    }
                    InitRules();
                    SetValuesInListViews(newRule);
                }
                else
                {
                    Utils.ShowError("Правило");
                }
            }
            ruleForm.Dispose();
        }

        private void btnEditRule_Click(object sender, System.EventArgs e)
        {
            if (lvRules.SelectedItems.Count > 0)
            {
                var oldRule = (Rule)lvRules.SelectedItems[0].Tag;
                var selectedRule = new Rule(oldRule);

                var addEditForm = new EditRuleForm(selectedRule, Instanse.BaseRule.VariableSet, Instanse.BaseRule.DomenSet, Instanse.BaseRule.RuleSet, EFormEnum.Edit);
                var dialog = addEditForm.ShowDialog();
                if (dialog == DialogResult.OK)
                {
                    var newRule = addEditForm.Rule;
                    // Изменили имя, а такое уже существует.
                    if (Instanse.BaseRule.RuleSet.Any<Rule>(x => x.Name == newRule.Name && oldRule.Uuid != newRule.Uuid))
                    {
                        Utils.ShowError("Правило");
                    }
                    else
                    {
                        Instanse.BaseRule.RuleSet.Remove(oldRule);
                        Instanse.BaseRule.RuleSet.Add(newRule);
                        InitRules();
                        SetValuesInListViews(newRule);
                    }
                }
            }
        }

        private void btnDeleteRule_Click(object sender, EventArgs e)
        {
            if (lvRules.SelectedItems.Count > 0)
            {
                var selectedRule = lvRules.SelectedItems[0].Tag as Rule;
                Instanse.BaseRule.RuleSet.Remove(selectedRule);
                InitRules();
                SetValuesInListViews();
            }
        }

        private void новыйToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Instanse = new ExpSysShell();
            InitRules();
            SetValuesInListViews();
        }

        private void открытьToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            try
            {
                var fileDialog = new OpenFileDialog
                {
                    InitialDirectory = Environment.CurrentDirectory,
                    Filter = "XML Files (*.xml)|*.xml",
                    DefaultExt = "xml",
                    FilterIndex = 0
                };
                var dialog = fileDialog.ShowDialog();

                if (dialog == DialogResult.OK || dialog == DialogResult.Yes)
                {
                    Instanse.BaseRule = Instanse.BaseRule.Load(fileDialog.FileName);
                    InitRules();
                    SetValuesInListViews();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Не удалось открыть ЭС, возможно, файл был поврежден");
            }
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileDialog = new SaveFileDialog();
            fileDialog.InitialDirectory = Environment.CurrentDirectory;
            fileDialog.Filter = "XML Files (*.xml)|*.xml";
            fileDialog.DefaultExt = "xml";
            fileDialog.FilterIndex = 0;
            var dialog = fileDialog.ShowDialog();

            if (dialog == DialogResult.OK || dialog == DialogResult.Yes)
            {
                Instanse.BaseRule.Save(fileDialog.FileName);
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void доменыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var domenSet = Instanse.BaseRule.DomenSet.Select(x => new Domen(x)).ToList();
            var variableSet = Instanse.BaseRule.VariableSet.Select(x => new Variable(x)).ToList();

            var domainForm = new DomenForm(domenSet, variableSet, Instanse.BaseRule.RuleSet);
            var domainFormDialog = domainForm.ShowDialog();
            if (domainFormDialog == DialogResult.OK)
            {
                foreach (var variable in Instanse.BaseRule.VariableSet)
                {
                    // Если переменной нет в новом наборе, то удаляем все правила, где она использовалась.
                    if (!domainForm.VariableSet.Any(x => x.Uuid == variable.Uuid))
                    {
                        Instanse.BaseRule.RuleSet.
                            RemoveAll(x =>
                            x.Consequences.Any(y => y.Variable.Uuid == variable.Uuid) ||
                            x.Premises.Any(y => y.Variable.Uuid == variable.Uuid));
                    }
                }
                Instanse.BaseRule.DomenSet = domainForm.DomenSet;
                Instanse.BaseRule.VariableSet = domainForm.VariableSet;
                // Обновление доменов у переменных.
                foreach (var variable in Instanse.BaseRule.VariableSet)
                {
                    var oldUuid = variable.Domen.Uuid;
                    variable.Domen = Instanse.BaseRule.DomenSet.Find(x => x.Uuid == oldUuid);
                }
                // Обновление переменных у фактов правил.
                foreach (var rule in Instanse.BaseRule.RuleSet)
                {
                    foreach (var fact in rule.Premises)
                    {
                        var oldUuid = fact.Variable.Uuid;
                        fact.Variable = Instanse.BaseRule.VariableSet.Find(x => x.Uuid == oldUuid);
                    }
                    foreach (var fact in rule.Consequences)
                    {
                        var oldUuid = fact.Variable.Uuid;
                        fact.Variable = Instanse.BaseRule.VariableSet.Find(x => x.Uuid == oldUuid);
                    }
                }
            }
            InitRules();
            domainForm.Dispose();
        }

        private void переменныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var domainSet = Instanse.BaseRule.DomenSet.Select(x => new Domen(x)).ToList();
            var variableSet = Instanse.BaseRule.VariableSet.Select(x => new Variable(x)).ToList();

            var variableForm = new VariableForm(variableSet, domainSet, Instanse.BaseRule.RuleSet);
            var variableFormDialog = variableForm.ShowDialog();

            if (variableFormDialog == DialogResult.OK)
            {
                // Если какие-то переменные были удалены, надо переписать правила, удалив нужные.
                foreach (var variable in Instanse.BaseRule.VariableSet)
                {
                    // Если переменной нет в новом наборе, то удаляем все правила, где она использовалась.
                    if (!variableForm.VariableSet.Any(x => x.Uuid == variable.Uuid))
                    {
                        Instanse.BaseRule.RuleSet.
                            RemoveAll(x => x.Consequences.Any(y => y.Variable.Uuid == variable.Uuid)
                            || x.Premises.Any(y => y.Variable.Uuid == variable.Uuid));
                    }
                }

                Instanse.BaseRule.VariableSet = variableForm.VariableSet;
                Instanse.BaseRule.DomenSet = variableForm.DomenSet;

                // Обновление доменов у переменных.
                foreach (var variable in Instanse.BaseRule.VariableSet)
                {
                    var oldUuid = variable.Domen.Uuid;
                    variable.Domen = Instanse.BaseRule.DomenSet.Find(x => x.Uuid == oldUuid);
                }
                // Обновление переменных у фактов правил.
                foreach (var rule in Instanse.BaseRule.RuleSet)
                {
                    foreach (var fact in rule.Premises)
                    {
                        var oldUuid = fact.Variable.Uuid;
                        fact.Variable = Instanse.BaseRule.VariableSet.Find(x => x.Uuid == oldUuid);
                    }
                    foreach (var fact in rule.Consequences)
                    {
                        var oldUuid = fact.Variable.Uuid;
                        fact.Variable = Instanse.BaseRule.VariableSet.Find(x => x.Uuid == oldUuid);
                    }
                }

            }
            InitRules();
            variableForm.Dispose();
        }

        private void задатьЦельКонсультацииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var goalForm = new GoalConsultForm(Instanse.BaseRule.VariableSet);
            var goalFormDialog = goalForm.ShowDialog();
            if (goalFormDialog == DialogResult.OK)
            {
                Instanse.Goal = goalForm.Goal;
            }
            goalForm.Dispose();
        }

        private void начатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Instanse.Goal != null)
            {
                Instanse.Consult();
            }
            else
            {
                MessageBox.Show("Не указана цель консультации!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                задатьЦельКонсультацииToolStripMenuItem_Click(sender, e);

                if (!string.IsNullOrWhiteSpace(Instanse.Goal.Name))
                {
                    Instanse.Consult();
                }
            }
        }

        private void получитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ExplainForm(Instanse.ChainUsedRules, Instanse.WorkMemory).ShowDialog();
        }

        private void lvRules_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvRules.SelectedItems.Count > 0)
            {
                var current = Instanse.BaseRule.RuleSet.First(x => x == (Rule)lvRules.SelectedItems[0].Tag);
                SetValuesInListViews(current);
            }
        }

        private void lvRules_ItemDrag(object sender, ItemDragEventArgs e)
        {
            DoDragDrop(e.Item, DragDropEffects.Copy);
        }

        private void lvRules_DoubleClick(object sender, EventArgs e)
        {
            btnEditRule_Click(sender, e);
        }

        private void lvRules_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                Point p = new Point(e.X, e.Y);
                Point point2 = lvRules.PointToClient(p);
                ListViewItem itemAt = lvRules.GetItemAt(point2.X, point2.Y);
                if (itemAt != null)
                {
                    var rule = ((Rule)data.Tag);
                    var newNumber = ((Rule)itemAt.Tag).Order;
                    rule.Order = newNumber;
                    Instanse.BaseRule.RuleSet.Remove(rule);
                    // Вставка на новое место
                    Instanse.BaseRule.RuleSet.Insert(rule.Order, rule);
                    // Пересчёт порядка у всех
                    for (int i = 0; i < Instanse.BaseRule.RuleSet.Count; i++)
                    {
                        if (Instanse.BaseRule.RuleSet[i].Order != i)
                        {
                            Instanse.BaseRule.RuleSet[i].Order = i;
                        }
                    }

                    InitRules();
                    SetValuesInListViews(rule);
                    lvRules.Items[rule.Order].Selected = true;
                }
            }
        }

        private void lvRules_DragEnter(object sender, DragEventArgs e)
        {
            Utils.DragEnter(sender, e);
        }

        private void lvRules_DragOver(object sender, DragEventArgs e)
        {
            Utils.DragOver(sender, e);
        }

        private void lvPremises_DragDrop(object sender, DragEventArgs e)
        {
            if (lvRules.SelectedItems.Count > 0)
            {
                var rule = (Rule)lvRules.SelectedItems[0].Tag;
                if (e.Data.GetDataPresent(typeof(ListViewItem)))
                {
                    ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                    Point p = new Point(e.X, e.Y);
                    Point point2 = lvPremises.PointToClient(p);
                    ListViewItem itemAt = lvPremises.GetItemAt(point2.X, point2.Y);
                    if (itemAt != null)
                    {
                        var fact = ((Fact)data.Tag);
                        var newNumber = ((Fact)itemAt.Tag).Order;
                        fact.Order = newNumber;
                        rule.Premises.Remove(fact);

                        // Вставка на новое место
                        rule.Premises.Insert(fact.Order, fact);

                        // Пересчёт порядка у всех
                        for (int i = 0; i < rule.Premises.Count; i++)
                        {
                            if (rule.Premises[i].Order != i)
                            {
                                rule.Premises[i].Order = i;
                            }
                        }

                        InitRules();
                        SetValuesInListViews(rule);
                        lvPremises.Items[rule.Order].Selected = true;
                    }
                }
            }
        }

        private void lvPremises_DragEnter(object sender, DragEventArgs e)
        {
            Utils.DragEnter(sender, e);
        }

        private void lvPremises_DragOver(object sender, DragEventArgs e)
        {
            Utils.DragOver(sender, e);
        }

        private void lvPremises_ItemDrag(object sender, ItemDragEventArgs e)
        {
            Utils.ItemDrag(this, sender, e);
        }

        private void lvConsequences_DragDrop(object sender, DragEventArgs e)
        {
            if (lvRules.SelectedItems.Count > 0)
            {
                var rule = (Rule)lvRules.SelectedItems[0].Tag;
                if (e.Data.GetDataPresent(typeof(ListViewItem)))
                {
                    ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                    Point p = new Point(e.X, e.Y);
                    Point point2 = lvConsequences.PointToClient(p);
                    ListViewItem itemAt = lvConsequences.GetItemAt(point2.X, point2.Y);
                    if (itemAt != null)
                    {
                        var fact = ((Fact)data.Tag);
                        var newNumber = ((Fact)itemAt.Tag).Order;
                        fact.Order = newNumber;
                        rule.Consequences.Remove(fact);

                        // Вставка на новое место
                        rule.Consequences.Insert(fact.Order, fact);

                        // Пересчёт порядка у всех
                        for (int i = 0; i < rule.Consequences.Count; i++)
                        {
                            if (rule.Consequences[i].Order != i)
                            {
                                rule.Consequences[i].Order = i;
                            }
                        }

                        InitRules();
                        SetValuesInListViews(rule);
                        lvConsequences.Items[rule.Order].Selected = true;
                    }
                }
            }
        }

        private void lvConsequences_DragEnter(object sender, DragEventArgs e)
        {
            Utils.DragEnter(sender, e);
        }

        private void lvConsequences_DragOver(object sender, DragEventArgs e)
        {
            Utils.DragOver(sender, e);
        }

        private void lvConsequences_ItemDrag(object sender, ItemDragEventArgs e)
        {
            Utils.ItemDrag(this, sender, e);
        }
    }
}