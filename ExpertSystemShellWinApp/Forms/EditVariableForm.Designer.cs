﻿namespace ExpertSystemShellWinApp.Forms
{
    partial class EditVariableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbNameVariable = new System.Windows.Forms.GroupBox();
            this.tbNameVariable = new System.Windows.Forms.TextBox();
            this.gbDomen = new System.Windows.Forms.GroupBox();
            this.btnAddDomen = new System.Windows.Forms.Button();
            this.btnEditDomen = new System.Windows.Forms.Button();
            this.lvDomen = new System.Windows.Forms.ListView();
            this.domenValNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.domenValValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmbDomen = new System.Windows.Forms.ComboBox();
            this.gbTypeVariable = new System.Windows.Forms.GroupBox();
            this.rbOutpRequested = new System.Windows.Forms.RadioButton();
            this.rbRequested = new System.Windows.Forms.RadioButton();
            this.rbVOutputed = new System.Windows.Forms.RadioButton();
            this.gbTextRequest = new System.Windows.Forms.GroupBox();
            this.tbQuestion = new System.Windows.Forms.TextBox();
            this.btnAddVariable = new System.Windows.Forms.Button();
            this.btnCancelVariable = new System.Windows.Forms.Button();
            this.gbNameVariable.SuspendLayout();
            this.gbDomen.SuspendLayout();
            this.gbTypeVariable.SuspendLayout();
            this.gbTextRequest.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbNameVariable
            // 
            this.gbNameVariable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbNameVariable.Controls.Add(this.tbNameVariable);
            this.gbNameVariable.Location = new System.Drawing.Point(15, 17);
            this.gbNameVariable.Name = "gbNameVariable";
            this.gbNameVariable.Size = new System.Drawing.Size(517, 55);
            this.gbNameVariable.TabIndex = 0;
            this.gbNameVariable.TabStop = false;
            this.gbNameVariable.Text = "Имя переменной";
            // 
            // tbNameVariable
            // 
            this.tbNameVariable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNameVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbNameVariable.Location = new System.Drawing.Point(18, 19);
            this.tbNameVariable.Name = "tbNameVariable";
            this.tbNameVariable.Size = new System.Drawing.Size(476, 21);
            this.tbNameVariable.TabIndex = 0;
            // 
            // gbDomen
            // 
            this.gbDomen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDomen.Controls.Add(this.btnAddDomen);
            this.gbDomen.Controls.Add(this.btnEditDomen);
            this.gbDomen.Controls.Add(this.lvDomen);
            this.gbDomen.Controls.Add(this.cmbDomen);
            this.gbDomen.Location = new System.Drawing.Point(15, 88);
            this.gbDomen.Name = "gbDomen";
            this.gbDomen.Size = new System.Drawing.Size(517, 197);
            this.gbDomen.TabIndex = 1;
            this.gbDomen.TabStop = false;
            this.gbDomen.Text = "Домен";
            // 
            // btnAddDomen
            // 
            this.btnAddDomen.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnAddDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddDomen.Location = new System.Drawing.Point(374, 19);
            this.btnAddDomen.Name = "btnAddDomen";
            this.btnAddDomen.Size = new System.Drawing.Size(137, 23);
            this.btnAddDomen.TabIndex = 4;
            this.btnAddDomen.Text = "Добавить домен";
            this.btnAddDomen.UseVisualStyleBackColor = true;
            this.btnAddDomen.Click += new System.EventHandler(this.btnAddDomen_Click_1);
            // 
            // btnEditDomen
            // 
            this.btnEditDomen.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnEditDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEditDomen.Location = new System.Drawing.Point(374, 48);
            this.btnEditDomen.Name = "btnEditDomen";
            this.btnEditDomen.Size = new System.Drawing.Size(137, 42);
            this.btnEditDomen.TabIndex = 3;
            this.btnEditDomen.Text = "Редактировать домен";
            this.btnEditDomen.UseVisualStyleBackColor = true;
            this.btnEditDomen.Click += new System.EventHandler(this.btnEditDomen_Click);
            // 
            // lvDomen
            // 
            this.lvDomen.AllowDrop = true;
            this.lvDomen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvDomen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.domenValNum,
            this.domenValValue});
            this.lvDomen.FullRowSelect = true;
            this.lvDomen.GridLines = true;
            this.lvDomen.Location = new System.Drawing.Point(18, 46);
            this.lvDomen.Name = "lvDomen";
            this.lvDomen.Size = new System.Drawing.Size(350, 131);
            this.lvDomen.TabIndex = 2;
            this.lvDomen.UseCompatibleStateImageBehavior = false;
            this.lvDomen.View = System.Windows.Forms.View.Details;
            this.lvDomen.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvDomen_ItemDrag);
            this.lvDomen.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvDomen_DragDrop);
            this.lvDomen.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvDomen_DragEnter);
            this.lvDomen.DragOver += new System.Windows.Forms.DragEventHandler(this.lvDomen_DragOver);
            // 
            // domenValNum
            // 
            this.domenValNum.Text = "№";
            // 
            // domenValValue
            // 
            this.domenValValue.Text = "Значение";
            this.domenValValue.Width = 243;
            // 
            // cmbDomen
            // 
            this.cmbDomen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDomen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbDomen.FormattingEnabled = true;
            this.cmbDomen.Location = new System.Drawing.Point(18, 19);
            this.cmbDomen.Name = "cmbDomen";
            this.cmbDomen.Size = new System.Drawing.Size(350, 23);
            this.cmbDomen.TabIndex = 0;
            this.cmbDomen.SelectedIndexChanged += new System.EventHandler(this.cmbDomen_SelectedIndexChanged);
            // 
            // gbTypeVariable
            // 
            this.gbTypeVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTypeVariable.Controls.Add(this.rbOutpRequested);
            this.gbTypeVariable.Controls.Add(this.rbRequested);
            this.gbTypeVariable.Controls.Add(this.rbVOutputed);
            this.gbTypeVariable.Location = new System.Drawing.Point(12, 291);
            this.gbTypeVariable.Name = "gbTypeVariable";
            this.gbTypeVariable.Size = new System.Drawing.Size(520, 92);
            this.gbTypeVariable.TabIndex = 2;
            this.gbTypeVariable.TabStop = false;
            this.gbTypeVariable.Text = "Вид переменной";
            // 
            // rbOutpRequested
            // 
            this.rbOutpRequested.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbOutpRequested.AutoSize = true;
            this.rbOutpRequested.Location = new System.Drawing.Point(18, 42);
            this.rbOutpRequested.Name = "rbOutpRequested";
            this.rbOutpRequested.Size = new System.Drawing.Size(163, 17);
            this.rbOutpRequested.TabIndex = 2;
            this.rbOutpRequested.Text = "Выводимо-запрашиваемая";
            this.rbOutpRequested.UseVisualStyleBackColor = true;
            this.rbOutpRequested.CheckedChanged += new System.EventHandler(this.rbOutpRequested_CheckedChanged);
            // 
            // rbRequested
            // 
            this.rbRequested.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbRequested.AutoSize = true;
            this.rbRequested.Location = new System.Drawing.Point(18, 65);
            this.rbRequested.Name = "rbRequested";
            this.rbRequested.Size = new System.Drawing.Size(108, 17);
            this.rbRequested.TabIndex = 1;
            this.rbRequested.Text = "Запрашиваемая";
            this.rbRequested.UseVisualStyleBackColor = true;
            this.rbRequested.CheckedChanged += new System.EventHandler(this.rbRequested_CheckedChanged);
            // 
            // rbVOutputed
            // 
            this.rbVOutputed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbVOutputed.AutoSize = true;
            this.rbVOutputed.Checked = true;
            this.rbVOutputed.Location = new System.Drawing.Point(18, 19);
            this.rbVOutputed.Name = "rbVOutputed";
            this.rbVOutputed.Size = new System.Drawing.Size(84, 17);
            this.rbVOutputed.TabIndex = 0;
            this.rbVOutputed.TabStop = true;
            this.rbVOutputed.Text = "Выводимая";
            this.rbVOutputed.UseVisualStyleBackColor = true;
            this.rbVOutputed.CheckedChanged += new System.EventHandler(this.rbVOutputed_CheckedChanged);
            // 
            // gbTextRequest
            // 
            this.gbTextRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTextRequest.Controls.Add(this.tbQuestion);
            this.gbTextRequest.Location = new System.Drawing.Point(12, 389);
            this.gbTextRequest.Name = "gbTextRequest";
            this.gbTextRequest.Size = new System.Drawing.Size(520, 87);
            this.gbTextRequest.TabIndex = 3;
            this.gbTextRequest.TabStop = false;
            this.gbTextRequest.Text = "Текст запроса";
            // 
            // tbQuestion
            // 
            this.tbQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbQuestion.Location = new System.Drawing.Point(18, 19);
            this.tbQuestion.Multiline = true;
            this.tbQuestion.Name = "tbQuestion";
            this.tbQuestion.Size = new System.Drawing.Size(479, 53);
            this.tbQuestion.TabIndex = 0;
            // 
            // btnAddVariable
            // 
            this.btnAddVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddVariable.Location = new System.Drawing.Point(15, 482);
            this.btnAddVariable.Name = "btnAddVariable";
            this.btnAddVariable.Size = new System.Drawing.Size(196, 23);
            this.btnAddVariable.TabIndex = 4;
            this.btnAddVariable.Text = "Добавить";
            this.btnAddVariable.UseVisualStyleBackColor = true;
            this.btnAddVariable.Click += new System.EventHandler(this.btnAddVariable_Click);
            // 
            // btnCancelVariable
            // 
            this.btnCancelVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCancelVariable.Location = new System.Drawing.Point(336, 482);
            this.btnCancelVariable.Name = "btnCancelVariable";
            this.btnCancelVariable.Size = new System.Drawing.Size(196, 23);
            this.btnCancelVariable.TabIndex = 5;
            this.btnCancelVariable.Text = "Отмена";
            this.btnCancelVariable.UseVisualStyleBackColor = true;
            this.btnCancelVariable.Click += new System.EventHandler(this.btnCancelVariable_Click);
            // 
            // EditVariableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 517);
            this.Controls.Add(this.btnCancelVariable);
            this.Controls.Add(this.btnAddVariable);
            this.Controls.Add(this.gbTextRequest);
            this.Controls.Add(this.gbTypeVariable);
            this.Controls.Add(this.gbDomen);
            this.Controls.Add(this.gbNameVariable);
            this.Name = "EditVariableForm";
            this.Text = "Редактирование переменной";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditVariableForm_FormClosing);
            this.gbNameVariable.ResumeLayout(false);
            this.gbNameVariable.PerformLayout();
            this.gbDomen.ResumeLayout(false);
            this.gbTypeVariable.ResumeLayout(false);
            this.gbTypeVariable.PerformLayout();
            this.gbTextRequest.ResumeLayout(false);
            this.gbTextRequest.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbNameVariable;
        private System.Windows.Forms.TextBox tbNameVariable;
        private System.Windows.Forms.GroupBox gbDomen;
        private System.Windows.Forms.Button btnEditDomen;
        private System.Windows.Forms.ListView lvDomen;
        private System.Windows.Forms.ColumnHeader domenValValue;
        private System.Windows.Forms.ComboBox cmbDomen;
        private System.Windows.Forms.GroupBox gbTypeVariable;
        private System.Windows.Forms.RadioButton rbVOutputed;
        private System.Windows.Forms.RadioButton rbOutpRequested;
        private System.Windows.Forms.RadioButton rbRequested;
        private System.Windows.Forms.GroupBox gbTextRequest;
        private System.Windows.Forms.TextBox tbQuestion;
        private System.Windows.Forms.Button btnAddVariable;
        private System.Windows.Forms.Button btnCancelVariable;
        private System.Windows.Forms.ColumnHeader domenValNum;
        private System.Windows.Forms.Button btnAddDomen;
    }
}