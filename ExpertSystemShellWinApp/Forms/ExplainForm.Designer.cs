﻿namespace ExpertSystemShellWinApp.Forms
{
    partial class ExplainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvWorkMemory = new System.Windows.Forms.TreeView();
            this.btnCollapseRules = new System.Windows.Forms.Button();
            this.btnExpandRules = new System.Windows.Forms.Button();
            this.lvWorkMemory = new System.Windows.Forms.ListView();
            this.columnVariable = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnVaue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbWorkMemory = new System.Windows.Forms.GroupBox();
            this.gbTrueRules = new System.Windows.Forms.GroupBox();
            this.gbWorkMemory.SuspendLayout();
            this.gbTrueRules.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvWorkMemory
            // 
            this.tvWorkMemory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvWorkMemory.Location = new System.Drawing.Point(9, 19);
            this.tvWorkMemory.Name = "tvWorkMemory";
            this.tvWorkMemory.Size = new System.Drawing.Size(696, 270);
            this.tvWorkMemory.TabIndex = 0;
            // 
            // btnCollapseRules
            // 
            this.btnCollapseRules.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCollapseRules.Location = new System.Drawing.Point(441, 318);
            this.btnCollapseRules.Name = "btnCollapseRules";
            this.btnCollapseRules.Size = new System.Drawing.Size(282, 23);
            this.btnCollapseRules.TabIndex = 1;
            this.btnCollapseRules.Text = "Свернуть правила";
            this.btnCollapseRules.UseVisualStyleBackColor = true;
            this.btnCollapseRules.Click += new System.EventHandler(this.btnCollapseRules_Click);
            // 
            // btnExpandRules
            // 
            this.btnExpandRules.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExpandRules.Location = new System.Drawing.Point(12, 318);
            this.btnExpandRules.Name = "btnExpandRules";
            this.btnExpandRules.Size = new System.Drawing.Size(282, 23);
            this.btnExpandRules.TabIndex = 2;
            this.btnExpandRules.Text = "Развернуть правила";
            this.btnExpandRules.UseVisualStyleBackColor = true;
            this.btnExpandRules.Click += new System.EventHandler(this.btnExpandRules_Click);
            // 
            // lvWorkMemory
            // 
            this.lvWorkMemory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvWorkMemory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnVariable,
            this.columnVaue});
            this.lvWorkMemory.FullRowSelect = true;
            this.lvWorkMemory.GridLines = true;
            this.lvWorkMemory.Location = new System.Drawing.Point(6, 19);
            this.lvWorkMemory.Name = "lvWorkMemory";
            this.lvWorkMemory.Size = new System.Drawing.Size(401, 303);
            this.lvWorkMemory.TabIndex = 3;
            this.lvWorkMemory.UseCompatibleStateImageBehavior = false;
            this.lvWorkMemory.View = System.Windows.Forms.View.Details;
            // 
            // columnVariable
            // 
            this.columnVariable.Text = "Переменная";
            this.columnVariable.Width = 108;
            // 
            // columnVaue
            // 
            this.columnVaue.Text = "Значение";
            this.columnVaue.Width = 281;
            // 
            // gbWorkMemory
            // 
            this.gbWorkMemory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbWorkMemory.Controls.Add(this.lvWorkMemory);
            this.gbWorkMemory.Location = new System.Drawing.Point(729, 13);
            this.gbWorkMemory.Name = "gbWorkMemory";
            this.gbWorkMemory.Size = new System.Drawing.Size(413, 328);
            this.gbWorkMemory.TabIndex = 4;
            this.gbWorkMemory.TabStop = false;
            this.gbWorkMemory.Text = "Рабочая память";
            // 
            // gbTrueRules
            // 
            this.gbTrueRules.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTrueRules.Controls.Add(this.tvWorkMemory);
            this.gbTrueRules.Location = new System.Drawing.Point(12, 13);
            this.gbTrueRules.Name = "gbTrueRules";
            this.gbTrueRules.Size = new System.Drawing.Size(711, 299);
            this.gbTrueRules.TabIndex = 5;
            this.gbTrueRules.TabStop = false;
            this.gbTrueRules.Text = "Сработавшие правила";
            // 
            // ExplainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1154, 351);
            this.Controls.Add(this.gbTrueRules);
            this.Controls.Add(this.btnCollapseRules);
            this.Controls.Add(this.gbWorkMemory);
            this.Controls.Add(this.btnExpandRules);
            this.Name = "ExplainForm";
            this.Text = "Объяснение";
            this.gbWorkMemory.ResumeLayout(false);
            this.gbTrueRules.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView tvWorkMemory;
        private System.Windows.Forms.Button btnCollapseRules;
        private System.Windows.Forms.Button btnExpandRules;
        private System.Windows.Forms.ListView lvWorkMemory;
        private System.Windows.Forms.GroupBox gbWorkMemory;
        private System.Windows.Forms.GroupBox gbTrueRules;
        private System.Windows.Forms.ColumnHeader columnVariable;
        private System.Windows.Forms.ColumnHeader columnVaue;
    }
}