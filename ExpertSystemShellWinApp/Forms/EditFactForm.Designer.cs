﻿namespace ExpertSystemShellWinApp.Forms
{
    partial class EditFactForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbCondition = new System.Windows.Forms.GroupBox();
            this.btnAddVariable = new System.Windows.Forms.Button();
            this.cmbValueVariable = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbVariable = new System.Windows.Forms.ComboBox();
            this.btnAddFact = new System.Windows.Forms.Button();
            this.btnCancelFact = new System.Windows.Forms.Button();
            this.gbCondition.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbCondition
            // 
            this.gbCondition.Controls.Add(this.btnAddVariable);
            this.gbCondition.Controls.Add(this.cmbValueVariable);
            this.gbCondition.Controls.Add(this.label1);
            this.gbCondition.Controls.Add(this.cmbVariable);
            this.gbCondition.Location = new System.Drawing.Point(10, 17);
            this.gbCondition.Name = "gbCondition";
            this.gbCondition.Size = new System.Drawing.Size(541, 56);
            this.gbCondition.TabIndex = 0;
            this.gbCondition.TabStop = false;
            this.gbCondition.Text = "Задание условия";
            // 
            // btnAddVariable
            // 
            this.btnAddVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddVariable.Location = new System.Drawing.Point(240, 17);
            this.btnAddVariable.Name = "btnAddVariable";
            this.btnAddVariable.Size = new System.Drawing.Size(31, 29);
            this.btnAddVariable.TabIndex = 3;
            this.btnAddVariable.Text = "+";
            this.btnAddVariable.UseVisualStyleBackColor = true;
            this.btnAddVariable.Click += new System.EventHandler(this.btnAddVariable_Click);
            // 
            // cmbValueVariable
            // 
            this.cmbValueVariable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbValueVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbValueVariable.FormattingEnabled = true;
            this.cmbValueVariable.Location = new System.Drawing.Point(307, 19);
            this.cmbValueVariable.Name = "cmbValueVariable";
            this.cmbValueVariable.Size = new System.Drawing.Size(228, 24);
            this.cmbValueVariable.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(277, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "==";
            // 
            // cmbVariable
            // 
            this.cmbVariable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbVariable.FormattingEnabled = true;
            this.cmbVariable.Location = new System.Drawing.Point(6, 19);
            this.cmbVariable.Name = "cmbVariable";
            this.cmbVariable.Size = new System.Drawing.Size(228, 24);
            this.cmbVariable.TabIndex = 0;
            this.cmbVariable.SelectedIndexChanged += new System.EventHandler(this.cmbVariable_SelectedIndexChanged);
            // 
            // btnAddFact
            // 
            this.btnAddFact.Location = new System.Drawing.Point(10, 89);
            this.btnAddFact.Name = "btnAddFact";
            this.btnAddFact.Size = new System.Drawing.Size(234, 23);
            this.btnAddFact.TabIndex = 1;
            this.btnAddFact.Text = "Добавить";
            this.btnAddFact.UseVisualStyleBackColor = true;
            this.btnAddFact.Click += new System.EventHandler(this.btnAddFact_Click);
            // 
            // btnCancelFact
            // 
            this.btnCancelFact.Location = new System.Drawing.Point(315, 89);
            this.btnCancelFact.Name = "btnCancelFact";
            this.btnCancelFact.Size = new System.Drawing.Size(236, 23);
            this.btnCancelFact.TabIndex = 2;
            this.btnCancelFact.Text = "Отмена";
            this.btnCancelFact.UseVisualStyleBackColor = true;
            this.btnCancelFact.Click += new System.EventHandler(this.btnCancelFact_Click);
            // 
            // EditFactForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 121);
            this.Controls.Add(this.btnCancelFact);
            this.Controls.Add(this.btnAddFact);
            this.Controls.Add(this.gbCondition);
            this.MaximumSize = new System.Drawing.Size(576, 160);
            this.MinimumSize = new System.Drawing.Size(576, 160);
            this.Name = "EditFactForm";
            this.Text = "Редактирование факта";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditFactForm_FormClosing);
            this.gbCondition.ResumeLayout(false);
            this.gbCondition.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCondition;
        private System.Windows.Forms.ComboBox cmbVariable;
        private System.Windows.Forms.ComboBox cmbValueVariable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddFact;
        private System.Windows.Forms.Button btnCancelFact;
        private System.Windows.Forms.Button btnAddVariable;
    }
}