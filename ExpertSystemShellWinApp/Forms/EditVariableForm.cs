﻿namespace ExpertSystemShellWinApp.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShellWinApp.Entities;

    public partial class EditVariableForm : Form
    {
        public Variable Variable { get; set; }
        public List<Domen> DomenSet { get; set; }
        public List<Rule> RuleSet { get; }
        public EFormEnum Type { get; set; }

        public EditVariableForm(int count, List<Domen> domenSet, List<Rule> ruleSet, EFormEnum type)
        {
            InitializeComponent();
            RuleSet = ruleSet;
            Variable = new Variable($"Variable_{count}", null, EVariableType.Derivable, string.Empty, Guid.NewGuid(), count);
            DomenSet = domenSet;
            Type = type;
            InitView();
        }

        public EditVariableForm(Variable variable, List<Domen> domenSet, List<Rule> ruleSet, EFormEnum type)
        {
            InitializeComponent();
            Variable = variable;
            RuleSet = ruleSet;
            DomenSet = domenSet;
            Type = type;
            InitView();
        }

        private void FillDomens()
        {
            lvDomen.Items.Clear();
            foreach (var domen in DomenSet.OrderBy(x => x.Order))
            {
                var item = new ListViewItem(new string[] { domen.Order.ToString(), domen.Name })
                {
                    Tag = domen
                };

                lvDomen.Items.Add(item);
            }
        }

        private void FillVariables(Domen currentDomen)
        {
            if (currentDomen != null)
            {
                foreach (var value in currentDomen.DomenValues.OrderBy(x => x.Order))
                {
                    var item = new ListViewItem(new string[] { value.Order.ToString(), value.Value })
                    {
                        Tag = value
                    };
                }
            }
        }

        private void InitView()
        {
            if (Type == EFormEnum.Add)
            {
                Text = "Добавление переменной";
                tbNameVariable.Text = Variable.Name;
                tbNameVariable.Focus();
                FillComboBox();
                if (DomenSet.Count > 0)
                {
                    /* пихаю его в переменную чтобы не падало */
                    Variable.Domen = DomenSet.OrderBy(x => x.Order).First();
                    FillList(Variable.Domen);
                }
                cmbDomen.Focus();
            }
            else
            {
                Text = "Редактирование переменной";
                tbNameVariable.Text = Variable.Name;
                cmbDomen.SelectedIndexChanged -= cmbDomen_SelectedIndexChanged;
                FillComboBox();
                cmbDomen.SelectedIndexChanged += cmbDomen_SelectedIndexChanged;
                cmbDomen.Focus();
            }

            switch (Variable.VariableType)
            {
                case EVariableType.Requested:
                    {
                        rbRequested.Checked = true;
                        tbQuestion.ReadOnly = false;
                        tbQuestion.Text = Variable.Question;
                    }
                    break;
                case EVariableType.DerivableRequested:
                    {
                        rbOutpRequested.Checked = true;
                        tbQuestion.ReadOnly = false;
                        tbQuestion.Text = Variable.Question;
                    }
                    break;
                case EVariableType.Derivable:
                    {
                        rbVOutputed.Checked = true;
                        tbQuestion.ReadOnly = true;
                        tbQuestion.Text = Variable.Question;
                    }
                    break;
            }
        }

        private void FillComboBox()
        {
            cmbDomen.DataSource = null;
            cmbDomen.DataSource = DomenSet;
            cmbDomen.DisplayMember = "Name";
            if (Variable.Domen != null)
            {
                cmbDomen.SelectedItem = DomenSet.FirstOrDefault(x => x.Uuid == Variable.Domen.Uuid);
                FillList(DomenSet.FirstOrDefault(x => x.Uuid == Variable.Domen.Uuid));
            }
        }

        private void FillList(Domen domen)
        {
            lvDomen.Items.Clear();
            if (domen != null)
            {
                foreach (var value in domen.DomenValues.OrderBy(x => x.Order))
                {
                    var item = new ListViewItem(new string[] { value.Order.ToString(), value.Value })
                    {
                        Tag = value
                    };

                    lvDomen.Items.Add(item);
                }
            }
        }

        private void btnAddDomen_Click(object sender, EventArgs e)
        {
            cmbDomen.SelectedItem = null;
        }

        private void btnEditDomen_Click(object sender, EventArgs e)
        {
            if (cmbDomen.SelectedItem != null)
            {
                var oldDomen = (Domen)cmbDomen.SelectedItem;
                var selectedDomen = new Domen(oldDomen);
                var editForm = new EditDomenForm(selectedDomen, EFormEnum.Edit);
                var dialog = editForm.ShowDialog();
                if (dialog == DialogResult.OK)
                {
                    var newDomen = editForm.Domen;
                    if (DomenSet.Any<Domen>(x => x.Name == newDomen.Name && x.Uuid != newDomen.Uuid))
                    {
                        Utils.ShowError("Домен");
                    }
                    else
                    {
                        bool isRemovedUsedValues = false;
                        foreach (var rule in RuleSet)
                        {
                            foreach (var fact in rule.Premises)
                            {
                                /* если среди значений домена нету использующегося в посылке, сравнение по имени */
                                if (fact.Variable.Domen.Uuid == newDomen.Uuid && !newDomen.DomenValues.Any(x => x.Value == fact.Value.Value))
                                {
                                    isRemovedUsedValues = true;
                                    break;
                                }
                            }
                            foreach (var fact in rule.Consequences)
                            {
                                /* если среди значений домена нету использующегося в заключении, сравнение по имени */
                                if (fact.Variable.Domen.Uuid == newDomen.Uuid && !newDomen.DomenValues.Any(x => x.Value == fact.Value.Value))
                                {
                                    isRemovedUsedValues = true;
                                    break;
                                }
                            }
                        }
                        if (isRemovedUsedValues)
                        {
                            /* спрашиваем о том, что вы удалили использующиеся значения, добавить ли новый */
                            var resultAnswer = MessageBox.Show("Были удалены использующиеся значения в домене! Создать новый домен?", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                            if (resultAnswer == DialogResult.OK)
                            {
                                /* если да меняем uuid и ставим на некст строчку с именем *_new */
                                newDomen.Name += "_new";
                                newDomen.Uuid = Guid.NewGuid();
                                newDomen.Order += 1;
                                DomenSet.Add(newDomen);
                                /* пересчитываем порядок */
                                for (int i = 0; i < DomenSet.Count; i++)
                                {
                                    if (DomenSet[i].Order != i)
                                    {
                                        DomenSet[i].Order = i;
                                    }
                                }
                            }
                        }
                        /* если не удалили использующееся значения, значит просто удаляем старый и добавляем новый */
                        else
                        {
                            DomenSet.Remove(oldDomen);
                            DomenSet.Add(newDomen);
                        }
                        Variable.Domen = newDomen;
                    }

                    cmbDomen.SelectedItem = DomenSet[DomenSet.Count - 1];
                }
                FillComboBox();
                FillList(Variable.Domen);
            }
        }

        private void EditVariableForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //MessageBox.Show("Сохранить данные?", "Информация", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
        }

        private void btnAddVariable_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbNameVariable.Text) || Variable.Domen == null || (Variable.VariableType == EVariableType.Requested && string.IsNullOrWhiteSpace(tbQuestion.Text)))
            {
                Hide();
                DialogResult = DialogResult.Cancel;
                if (Variable.VariableType == EVariableType.Requested && string.IsNullOrWhiteSpace(tbQuestion.Text))
                {
                    MessageBox.Show("Пустое текст вопроса запрашиваемой переменной!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (string.IsNullOrWhiteSpace(tbNameVariable.Text))
                {
                    MessageBox.Show("Пустое имя переменной!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else /* пустой домен */
                {
                    MessageBox.Show("Не задан домен переменной!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                DialogResult = DialogResult.OK;
                Variable.Question = tbQuestion.Text.Trim();
                Variable.Name = tbNameVariable.Text.Trim();
            }

            Close();
        }

        private void btnCancelVariable_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void lvDomen_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                Point p = new Point(e.X, e.Y);
                Point point2 = lvDomen.PointToClient(p);
                ListViewItem itemAt = lvDomen.GetItemAt(point2.X, point2.Y);
                if (itemAt != null)
                {
                    var selectedDomen = cmbDomen.SelectedItem as Domen;
                    var domenValue = ((DomenVal)data.Tag);
                    var newNumber = ((DomenVal)itemAt.Tag).Order;
                    domenValue.Order = newNumber;
                    selectedDomen.DomenValues.RemoveAll(x => x.Value == domenValue.Value);

                    // Вставка на новое место
                    selectedDomen.DomenValues.Insert(domenValue.Order, domenValue);

                    // Пересчёт порядка у всех
                    for (int i = 0; i < selectedDomen.DomenValues.Count; i++)
                    {
                        if (selectedDomen.DomenValues[i].Order != i)
                        {
                            selectedDomen.DomenValues[i].Order = i;
                        }
                    }

                    Variable.Domen = selectedDomen;
                    FillList(selectedDomen);
                    lvDomen.Items[domenValue.Order].Selected = true;
                }
            }
        }

        private void lvDomen_DragEnter(object sender, DragEventArgs e)
        {
            Utils.DragEnter(sender, e);
        }

        private void lvDomen_DragOver(object sender, DragEventArgs e)
        {
            Utils.DragOver(sender, e);
        }

        private void CmbDomenName_SelectedIndexChanged(object sender, EventArgs e)
        {
            var currentDomen = (Domen)cmbDomen.SelectedItem;
            FillList(currentDomen);
            Variable.Domen = currentDomen;
        }

        private void lvDomen_ItemDrag(object sender, ItemDragEventArgs e)
        {
            Utils.ItemDrag(this, sender, e);
        }

        private void rbVOutputed_CheckedChanged(object sender, EventArgs e)
        {
            var radiuButton = sender as RadioButton;
            Variable.VariableType = Utils.DictVarialbeType.FirstOrDefault(x => x.Value == radiuButton.Text).Key;
            if (Variable.VariableType != EVariableType.Derivable)
            {
                tbQuestion.ReadOnly = false;
            }
            else
            {
                tbQuestion.Text = string.Empty;
                tbQuestion.ReadOnly = true;
            }

            tbQuestion.Text = "";
        }

        private void rbOutpRequested_CheckedChanged(object sender, EventArgs e)
        {
            var radiuButton = sender as RadioButton;
            Variable.VariableType = Utils.DictVarialbeType.FirstOrDefault(x => x.Value == radiuButton.Text).Key;
            if (Variable.VariableType != EVariableType.Derivable)
            {
                tbQuestion.ReadOnly = false;
            }
            else
            {
                tbQuestion.Text = string.Empty;
                tbQuestion.ReadOnly = true;
            }

            tbQuestion.Text = tbNameVariable.Text + "?";
            tbQuestion.Focus();
        }

        private void rbRequested_CheckedChanged(object sender, EventArgs e)
        {
            var radiuButton = sender as RadioButton;
            Variable.VariableType = Utils.DictVarialbeType.FirstOrDefault(x => x.Value == radiuButton.Text).Key;
            if (Variable.VariableType != EVariableType.Derivable)
            {
                tbQuestion.ReadOnly = false;
            }
            else
            {
                tbQuestion.Text = string.Empty;
                tbQuestion.ReadOnly = true;
            }

            tbQuestion.Text = tbNameVariable.Text + "?";
            tbQuestion.Focus();
        }

        private void cmbDomen_SelectedIndexChanged(object sender, EventArgs e)
        {
            var currentDomen = (Domen)cmbDomen.SelectedItem;
            FillList(currentDomen);
            Variable.Domen = currentDomen;
        }

        private void btnAddDomen_Click_1(object sender, EventArgs e)
        {
            var editDomenForm = new EditDomenForm(DomenSet.Count, EFormEnum.Add);
            var editDialog = editDomenForm.ShowDialog();
            if (editDialog == DialogResult.OK)
            {
                var newDomen = editDomenForm.Domen;
                if (DomenSet.Any<Domen>(x => x.Name == newDomen.Name))
                {
                    Utils.ShowError("Домен");
                }
                else
                {
                    if (lvDomen.SelectedItems.Count > 0)
                    {
                        var number = ((Domen)lvDomen.SelectedItems[0].Tag).Order + 1;
                        DomenSet.Insert(number, newDomen);
                        //пересчитываю порядок у всех
                        for (int i = 0; i < DomenSet.Count; i++)
                        {
                            if (DomenSet[i].Order != i)
                            {
                                DomenSet[i].Order = i;
                            }
                        }
                    }
                    else
                    {
                        DomenSet.Add(newDomen);
                    }
                    FillDomens();
                    FillVariables(newDomen);
                }

                FillComboBox();
                cmbDomen.SelectedItem = DomenSet[DomenSet.Count - 1];
            }

            editDomenForm.Dispose();
        }
    }
}
