﻿namespace ExpertSystemShellWinApp.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuSt = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новыйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьКакToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.знанияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.доменыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.переменныеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.консультацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.задатьЦельКонсультацииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.начатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.объяснениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.получитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnEditRule = new System.Windows.Forms.Button();
            this.btnDeleteRule = new System.Windows.Forms.Button();
            this.btnAddRule = new System.Windows.Forms.Button();
            this.lvRules = new System.Windows.Forms.ListView();
            this.nameRule = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contentRule = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbActions = new System.Windows.Forms.GroupBox();
            this.lvPremises = new System.Windows.Forms.ListView();
            this.condition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvConsequences = new System.Windows.Forms.ListView();
            this.сonclusion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbCurrentRule = new System.Windows.Forms.GroupBox();
            this.gbRules = new System.Windows.Forms.GroupBox();
            this.menuSt.SuspendLayout();
            this.gbActions.SuspendLayout();
            this.gbCurrentRule.SuspendLayout();
            this.gbRules.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuSt
            // 
            this.menuSt.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.знанияToolStripMenuItem,
            this.консультацияToolStripMenuItem,
            this.объяснениеToolStripMenuItem});
            this.menuSt.Location = new System.Drawing.Point(0, 0);
            this.menuSt.Name = "menuSt";
            this.menuSt.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuSt.Size = new System.Drawing.Size(1101, 24);
            this.menuSt.TabIndex = 1;
            this.menuSt.Text = "menuSt";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новыйToolStripMenuItem,
            this.открытьToolStripMenuItem,
            this.сохранитьКакToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // новыйToolStripMenuItem
            // 
            this.новыйToolStripMenuItem.Name = "новыйToolStripMenuItem";
            this.новыйToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.новыйToolStripMenuItem.Text = "Новый";
            this.новыйToolStripMenuItem.Click += new System.EventHandler(this.новыйToolStripMenuItem_Click);
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // сохранитьКакToolStripMenuItem
            // 
            this.сохранитьКакToolStripMenuItem.Name = "сохранитьКакToolStripMenuItem";
            this.сохранитьКакToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.сохранитьКакToolStripMenuItem.Text = "Сохранить как";
            this.сохранитьКакToolStripMenuItem.Click += new System.EventHandler(this.сохранитьКакToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // знанияToolStripMenuItem
            // 
            this.знанияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.доменыToolStripMenuItem,
            this.переменныеToolStripMenuItem});
            this.знанияToolStripMenuItem.Name = "знанияToolStripMenuItem";
            this.знанияToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.знанияToolStripMenuItem.Text = "Знания";
            // 
            // доменыToolStripMenuItem
            // 
            this.доменыToolStripMenuItem.Name = "доменыToolStripMenuItem";
            this.доменыToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.доменыToolStripMenuItem.Text = "Домены";
            this.доменыToolStripMenuItem.Click += new System.EventHandler(this.доменыToolStripMenuItem_Click);
            // 
            // переменныеToolStripMenuItem
            // 
            this.переменныеToolStripMenuItem.Name = "переменныеToolStripMenuItem";
            this.переменныеToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.переменныеToolStripMenuItem.Text = "Переменные";
            this.переменныеToolStripMenuItem.Click += new System.EventHandler(this.переменныеToolStripMenuItem_Click);
            // 
            // консультацияToolStripMenuItem
            // 
            this.консультацияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.задатьЦельКонсультацииToolStripMenuItem,
            this.начатьToolStripMenuItem});
            this.консультацияToolStripMenuItem.Name = "консультацияToolStripMenuItem";
            this.консультацияToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.консультацияToolStripMenuItem.Text = "Консультация";
            // 
            // задатьЦельКонсультацииToolStripMenuItem
            // 
            this.задатьЦельКонсультацииToolStripMenuItem.Name = "задатьЦельКонсультацииToolStripMenuItem";
            this.задатьЦельКонсультацииToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.задатьЦельКонсультацииToolStripMenuItem.Text = "Задать цель консультации";
            this.задатьЦельКонсультацииToolStripMenuItem.Click += new System.EventHandler(this.задатьЦельКонсультацииToolStripMenuItem_Click);
            // 
            // начатьToolStripMenuItem
            // 
            this.начатьToolStripMenuItem.Name = "начатьToolStripMenuItem";
            this.начатьToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.начатьToolStripMenuItem.Text = "Начать";
            this.начатьToolStripMenuItem.Click += new System.EventHandler(this.начатьToolStripMenuItem_Click);
            // 
            // объяснениеToolStripMenuItem
            // 
            this.объяснениеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.получитьToolStripMenuItem});
            this.объяснениеToolStripMenuItem.Name = "объяснениеToolStripMenuItem";
            this.объяснениеToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.объяснениеToolStripMenuItem.Text = "Объяснение";
            // 
            // получитьToolStripMenuItem
            // 
            this.получитьToolStripMenuItem.Name = "получитьToolStripMenuItem";
            this.получитьToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.получитьToolStripMenuItem.Text = "Получить";
            this.получитьToolStripMenuItem.Click += new System.EventHandler(this.получитьToolStripMenuItem_Click);
            // 
            // btnEditRule
            // 
            this.btnEditRule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditRule.Location = new System.Drawing.Point(16, 56);
            this.btnEditRule.Name = "btnEditRule";
            this.btnEditRule.Size = new System.Drawing.Size(349, 26);
            this.btnEditRule.TabIndex = 3;
            this.btnEditRule.Text = "Редактировать";
            this.btnEditRule.UseVisualStyleBackColor = true;
            this.btnEditRule.Click += new System.EventHandler(this.btnEditRule_Click);
            // 
            // btnDeleteRule
            // 
            this.btnDeleteRule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteRule.Location = new System.Drawing.Point(16, 88);
            this.btnDeleteRule.Name = "btnDeleteRule";
            this.btnDeleteRule.Size = new System.Drawing.Size(349, 26);
            this.btnDeleteRule.TabIndex = 4;
            this.btnDeleteRule.Text = "Удалить";
            this.btnDeleteRule.UseVisualStyleBackColor = true;
            this.btnDeleteRule.Click += new System.EventHandler(this.btnDeleteRule_Click);
            // 
            // btnAddRule
            // 
            this.btnAddRule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddRule.Location = new System.Drawing.Point(16, 24);
            this.btnAddRule.Name = "btnAddRule";
            this.btnAddRule.Size = new System.Drawing.Size(349, 26);
            this.btnAddRule.TabIndex = 8;
            this.btnAddRule.Text = "Добавить";
            this.btnAddRule.UseVisualStyleBackColor = true;
            this.btnAddRule.Click += new System.EventHandler(this.btnAddRule_Click);
            // 
            // lvRules
            // 
            this.lvRules.AllowDrop = true;
            this.lvRules.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvRules.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameRule,
            this.contentRule});
            this.lvRules.FullRowSelect = true;
            this.lvRules.GridLines = true;
            this.lvRules.Location = new System.Drawing.Point(7, 19);
            this.lvRules.Name = "lvRules";
            this.lvRules.Size = new System.Drawing.Size(649, 365);
            this.lvRules.TabIndex = 0;
            this.lvRules.UseCompatibleStateImageBehavior = false;
            this.lvRules.View = System.Windows.Forms.View.Details;
            this.lvRules.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvRules_ItemDrag);
            this.lvRules.SelectedIndexChanged += new System.EventHandler(this.lvRules_SelectedIndexChanged);
            this.lvRules.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvRules_DragDrop);
            this.lvRules.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvRules_DragEnter);
            this.lvRules.DragOver += new System.Windows.Forms.DragEventHandler(this.lvRules_DragOver);
            this.lvRules.DoubleClick += new System.EventHandler(this.lvRules_DoubleClick);
            // 
            // nameRule
            // 
            this.nameRule.Text = "Имя правила";
            this.nameRule.Width = 109;
            // 
            // contentRule
            // 
            this.contentRule.Text = "Содержание правила";
            this.contentRule.Width = 484;
            // 
            // gbActions
            // 
            this.gbActions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbActions.Controls.Add(this.btnAddRule);
            this.gbActions.Controls.Add(this.btnDeleteRule);
            this.gbActions.Controls.Add(this.btnEditRule);
            this.gbActions.Location = new System.Drawing.Point(697, 27);
            this.gbActions.Name = "gbActions";
            this.gbActions.Size = new System.Drawing.Size(380, 124);
            this.gbActions.TabIndex = 10;
            this.gbActions.TabStop = false;
            this.gbActions.Text = "Действия";
            // 
            // lvPremises
            // 
            this.lvPremises.AllowDrop = true;
            this.lvPremises.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lvPremises.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.condition});
            this.lvPremises.FullRowSelect = true;
            this.lvPremises.GridLines = true;
            this.lvPremises.Location = new System.Drawing.Point(16, 20);
            this.lvPremises.Name = "lvPremises";
            this.lvPremises.Size = new System.Drawing.Size(349, 117);
            this.lvPremises.TabIndex = 11;
            this.lvPremises.UseCompatibleStateImageBehavior = false;
            this.lvPremises.View = System.Windows.Forms.View.Details;
            this.lvPremises.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvPremises_ItemDrag);
            this.lvPremises.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvPremises_DragDrop);
            this.lvPremises.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvPremises_DragEnter);
            this.lvPremises.DragOver += new System.Windows.Forms.DragEventHandler(this.lvPremises_DragOver);
            // 
            // condition
            // 
            this.condition.Text = "Условие";
            this.condition.Width = 341;
            // 
            // lvConsequences
            // 
            this.lvConsequences.AllowDrop = true;
            this.lvConsequences.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lvConsequences.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.сonclusion});
            this.lvConsequences.FullRowSelect = true;
            this.lvConsequences.GridLines = true;
            this.lvConsequences.Location = new System.Drawing.Point(16, 143);
            this.lvConsequences.Name = "lvConsequences";
            this.lvConsequences.Size = new System.Drawing.Size(349, 111);
            this.lvConsequences.TabIndex = 12;
            this.lvConsequences.UseCompatibleStateImageBehavior = false;
            this.lvConsequences.View = System.Windows.Forms.View.Details;
            this.lvConsequences.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvConsequences_ItemDrag);
            this.lvConsequences.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvConsequences_DragDrop);
            this.lvConsequences.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvConsequences_DragEnter);
            this.lvConsequences.DragOver += new System.Windows.Forms.DragEventHandler(this.lvConsequences_DragOver);
            // 
            // сonclusion
            // 
            this.сonclusion.Text = "Заключение";
            this.сonclusion.Width = 338;
            // 
            // gbCurrentRule
            // 
            this.gbCurrentRule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCurrentRule.Controls.Add(this.lvConsequences);
            this.gbCurrentRule.Controls.Add(this.lvPremises);
            this.gbCurrentRule.Location = new System.Drawing.Point(697, 157);
            this.gbCurrentRule.Name = "gbCurrentRule";
            this.gbCurrentRule.Size = new System.Drawing.Size(380, 265);
            this.gbCurrentRule.TabIndex = 13;
            this.gbCurrentRule.TabStop = false;
            this.gbCurrentRule.Text = "Текущее правило";
            // 
            // gbRules
            // 
            this.gbRules.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbRules.Controls.Add(this.lvRules);
            this.gbRules.Location = new System.Drawing.Point(12, 27);
            this.gbRules.Name = "gbRules";
            this.gbRules.Size = new System.Drawing.Size(671, 395);
            this.gbRules.TabIndex = 9;
            this.gbRules.TabStop = false;
            this.gbRules.Text = "Правила";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1101, 440);
            this.Controls.Add(this.gbCurrentRule);
            this.Controls.Add(this.gbActions);
            this.Controls.Add(this.gbRules);
            this.Controls.Add(this.menuSt);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuSt;
            this.Name = "MainForm";
            this.Text = "Оболочка ЭС";
            this.menuSt.ResumeLayout(false);
            this.menuSt.PerformLayout();
            this.gbActions.ResumeLayout(false);
            this.gbCurrentRule.ResumeLayout(false);
            this.gbRules.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuSt;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новыйToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьКакToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem знанияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem доменыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem переменныеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem консультацияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem начатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem объяснениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem получитьToolStripMenuItem;
        private System.Windows.Forms.Button btnEditRule;
        private System.Windows.Forms.Button btnDeleteRule;
        private System.Windows.Forms.Button btnAddRule;
        private System.Windows.Forms.ListView lvRules;
        private System.Windows.Forms.ColumnHeader nameRule;
        private System.Windows.Forms.ColumnHeader contentRule;
        private System.Windows.Forms.GroupBox gbActions;
        private System.Windows.Forms.ListView lvPremises;
        private System.Windows.Forms.ListView lvConsequences;
        private System.Windows.Forms.GroupBox gbCurrentRule;
        private System.Windows.Forms.ColumnHeader condition;
        private System.Windows.Forms.ColumnHeader сonclusion;
        private System.Windows.Forms.ToolStripMenuItem задатьЦельКонсультацииToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbRules;
    }
}

