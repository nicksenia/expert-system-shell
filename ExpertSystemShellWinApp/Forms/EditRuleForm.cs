﻿namespace ExpertSystemShellWinApp.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShellWinApp.Entities;

    public partial class EditRuleForm : Form
    {
        public Rule Rule { get; set; }
        public List<Variable> VariableSet { get; set; }
        public List<Domen> DomenSet { get; set; }
        public List<Rule> RuleSet { get; set; }
        EFormEnum Type { get; set; }
        public EditRuleForm(Rule rule, List<Variable> variableSet, List<Domen> domenSet, List<Rule> ruleSet, EFormEnum type)
        {
            InitializeComponent();
            Rule = rule;
            VariableSet = variableSet;
            DomenSet = domenSet;
            RuleSet = ruleSet;
            Type = type;
            InitView();
        }

        public EditRuleForm(int count, List<Variable> variableSet, List<Domen> domenSet, List<Rule> ruleSet, EFormEnum type)
        {
            InitializeComponent();
            Rule = new Rule
            {
                Name = $"Rule_{count}",
                Premises = new List<Fact>(),
                Consequences = new List<Fact>(),
                Order = count,
                Reason = string.Empty
            };

            VariableSet = variableSet;
            DomenSet = domenSet;
            RuleSet = ruleSet;
            Type = type;
            InitView();
        }

        private void InitView()
        {
            if (Type == EFormEnum.Add)
            {
                Text = "Добавление правила";
                tbNameRule.Text = Rule.Name;
                tbNameRule.Focus();
            }
            else
            {
                Text = "Редактирование правила";
                tbNameRule.Text = Rule.Name;
                tbReason.Text = Rule.Reason;
                FillValues();
            }
        }

        private void FillValues()
        {
            lvPremises.Items.Clear();
            lvConsequences.Items.Clear();

            foreach (var value in Rule.Premises.OrderBy(x => x.Order))
            {
                var item = new ListViewItem(new string[] { value.Order.ToString(), value.ToString() })
                {
                    Tag = value
                };

                lvPremises.Items.Add(item);
            }

            foreach (var value in Rule.Consequences.OrderBy(x => x.Order))
            {
                var item = new ListViewItem(new string[] { value.Order.ToString(), value.ToString() })
                {
                    Tag = value
                };

                lvConsequences.Items.Add(item);
            }
        }

        private void EditRuleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //MessageBox.Show("Сохранить данные?", "Информация", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Hide();
            if (!string.IsNullOrWhiteSpace(tbNameRule.Text))
            {
                Rule.Name = tbNameRule.Text;
                Rule.Reason = tbReason.Text;
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Не задано имя правила!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DialogResult = DialogResult.Cancel;
            }

            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void lvPremises_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnEditPremises_Click(sender, e);
        }

        private void btnAddPremises_Click(object sender, EventArgs e)
        {
            var editFactForm = new EditFactForm(Rule.Premises.Count, VariableSet, DomenSet, RuleSet, EFormEnum.Add, false);
            var editDialog = editFactForm.ShowDialog();
            if (editDialog == DialogResult.OK)
            {
                var newFact = editFactForm.Fact;
                if (!Rule.Premises.Any(x => x.ToString() == newFact.ToString()))
                {
                    Rule.Premises.Add(newFact);
                    FillValues();
                }
                else
                {
                    MessageBox.Show("Данный факт уже добавлен!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            editFactForm.Dispose();
        }

        private void btnEditPremises_Click(object sender, EventArgs e)
        {
            if (lvPremises.SelectedItems.Count > 0)
            {
                var oldFact = (Fact)lvPremises.SelectedItems[0].Tag;
                var selectedFact = new Fact(oldFact);
                var editFactForm = new EditFactForm(selectedFact, VariableSet, DomenSet, RuleSet, EFormEnum.Edit, false);
                var editDialog = editFactForm.ShowDialog();
                if (editDialog == DialogResult.OK)
                {
                    var newFact = editFactForm.Fact;
                    Rule.Premises.Remove(oldFact);
                    Rule.Premises.Add(newFact);
                    FillValues();
                }
                editFactForm.Dispose();
            }
        }

        private void btnDeletePremises_Click(object sender, EventArgs e)
        {
            if (lvPremises.SelectedItems.Count > 0)
            {
                var oldFact = (Fact)lvPremises.SelectedItems[0].Tag;
                Rule.Premises.Remove(oldFact);
                lvPremises.Items.Remove(lvPremises.SelectedItems[0]);
                for (int i = 0; i < Rule.Premises.Count; i++)
                {
                    ListViewItem item = null;
                    for (int j = 0; j < lvPremises.Items.Count; j++)
                    {
                        if (((Fact)lvPremises.Items[j].Tag).Value == Rule.Premises[i].Value)
                        {
                            item = lvPremises.Items[j];
                        }
                    }
                    Rule.Premises[i].Order = lvPremises.Items.IndexOf(item);
                }
                FillValues();
            }
        }

        private void btnAddConsequences_Click(object sender, EventArgs e)
        {
            var editFactForm = new EditFactForm(Rule.Consequences.Count, VariableSet, DomenSet, RuleSet, EFormEnum.Add, true);
            var editDialog = editFactForm.ShowDialog();
            if (editDialog == DialogResult.OK)
            {
                var newFact = editFactForm.Fact;
                /* ради нормальной работы одинаковые условия не пихаем */
                if (!Rule.Consequences.Any(x => x.ToString() == newFact.ToString()))
                {
                    Rule.Consequences.Add(newFact);
                    FillValues();
                }
                else
                {
                    MessageBox.Show("Данный факт уже добавлен!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            editFactForm.Dispose();
        }

        private void btnEditConsequences_Click(object sender, EventArgs e)
        {
            if (lvConsequences.SelectedItems.Count > 0)
            {
                var oldFact = (Fact)lvConsequences.SelectedItems[0].Tag;
                var selectedFact = new Fact(oldFact);
                var editFactForm = new EditFactForm(selectedFact, VariableSet, DomenSet, RuleSet, EFormEnum.Edit, true);
                var editDialog = editFactForm.ShowDialog();
                if (editDialog == DialogResult.OK)
                {
                    var newFact = editFactForm.Fact;
                    Rule.Consequences.Remove(oldFact);
                    Rule.Consequences.Add(newFact);
                    FillValues();
                }
                editFactForm.Dispose();
            }
        }

        private void btnDeleteConsequences_Click(object sender, EventArgs e)
        {
            if (lvConsequences.SelectedItems.Count > 0)
            {
                var oldFact = (Fact)lvConsequences.SelectedItems[0].Tag;
                Rule.Consequences.Remove(oldFact);
                /*надо обновить номера, но перед этим удалить нужную строчку*/
                lvConsequences.Items.Remove(lvConsequences.SelectedItems[0]);
                for (int i = 0; i < Rule.Consequences.Count; i++)
                {
                    ListViewItem item = null;
                    for (int j = 0; j < lvConsequences.Items.Count; j++)
                    {
                        if (((Fact)lvConsequences.Items[j].Tag).Value == Rule.Consequences[i].Value)
                        {
                            item = lvConsequences.Items[j];
                        }
                    }
                    Rule.Consequences[i].Order = lvConsequences.Items.IndexOf(item);
                }
                FillValues();
            }
        }

        private void lvConsequences_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnDeleteConsequences_Click(sender, e);
        }

        private void lvPremises_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                Point p = new Point(e.X, e.Y);
                Point point2 = lvPremises.PointToClient(p);
                ListViewItem itemAt = lvPremises.GetItemAt(point2.X, point2.Y);
                if (itemAt != null)
                {
                    var fact = ((Fact)data.Tag);
                    var newNumber = ((Fact)itemAt.Tag).Order;
                    fact.Order = newNumber;
                    Rule.Premises.Remove(fact);

                    // Вставка на новое место
                    Rule.Premises.Insert(fact.Order, fact);

                    // Пересчёт порядка у всех
                    for (int i = 0; i < Rule.Premises.Count; i++)
                    {
                        if (Rule.Premises[i].Order != i)
                        {
                            Rule.Premises[i].Order = i;
                        }
                    }

                    FillValues();
                    lvPremises.Items[fact.Order].Selected = true;
                }
            }
        }

        private void lvPremises_DragEnter(object sender, DragEventArgs e)
        {
            Utils.DragEnter(sender, e);
        }

        private void lvPremises_DragOver(object sender, DragEventArgs e)
        {
            Utils.DragOver(sender, e);
        }

        private void lvPremises_ItemDrag(object sender, ItemDragEventArgs e)
        {
            Utils.ItemDrag(this, sender, e);
        }

        private void lvConsequences_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                Point p = new Point(e.X, e.Y);
                Point point2 = lvConsequences.PointToClient(p);
                ListViewItem itemAt = lvConsequences.GetItemAt(point2.X, point2.Y);
                if (itemAt != null)
                {
                    //беру выбранный факт
                    var fact = ((Fact)data.Tag);
                    //беру его новый номер
                    var newNumber = ((Fact)itemAt.Tag).Order;
                    fact.Order = newNumber;
                    //удаляю выбранный факт
                    Rule.Consequences.Remove(fact);
                    //вставляю его на новое место = новый номер
                    Rule.Consequences.Insert(fact.Order, fact);
                    //пересчитываю порядок у всех
                    for (int i = 0; i < Rule.Consequences.Count; i++)
                    {
                        if (Rule.Consequences[i].Order != i)
                        {
                            Rule.Consequences[i].Order = i;
                        }
                    }
                    FillValues();

                    lvConsequences.Items[fact.Order].Selected = true;
                }
            }
        }

        private void lvConsequences_DragEnter(object sender, DragEventArgs e)
        {
            Utils.DragEnter(sender, e);
        }

        private void lvConsequences_DragOver(object sender, DragEventArgs e)
        {
            Utils.DragOver(sender, e);
        }

        private void lvConsequences_ItemDrag(object sender, ItemDragEventArgs e)
        {
            Utils.ItemDrag(this, sender, e);
        }
    }
}
