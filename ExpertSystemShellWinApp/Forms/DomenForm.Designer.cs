﻿namespace ExpertSystemShellWinApp.Forms
{
    partial class DomenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbDomen = new System.Windows.Forms.GroupBox();
            this.lvDomen = new System.Windows.Forms.ListView();
            this.domenIndex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.domenName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbActions = new System.Windows.Forms.GroupBox();
            this.btnDeleteDomen = new System.Windows.Forms.Button();
            this.btnEditDomen = new System.Windows.Forms.Button();
            this.btnAddDomen = new System.Windows.Forms.Button();
            this.gbCurrentDomen = new System.Windows.Forms.GroupBox();
            this.lvCurrentDomen = new System.Windows.Forms.ListView();
            this.domenValueIndex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.domenValueName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnSaveAndClose = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.gbDomen.SuspendLayout();
            this.gbActions.SuspendLayout();
            this.gbCurrentDomen.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDomen
            // 
            this.gbDomen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDomen.Controls.Add(this.lvDomen);
            this.gbDomen.Location = new System.Drawing.Point(11, 10);
            this.gbDomen.Name = "gbDomen";
            this.gbDomen.Size = new System.Drawing.Size(355, 385);
            this.gbDomen.TabIndex = 0;
            this.gbDomen.TabStop = false;
            this.gbDomen.Text = "Домены";
            // 
            // lvDomen
            // 
            this.lvDomen.AllowDrop = true;
            this.lvDomen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvDomen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.domenIndex,
            this.domenName});
            this.lvDomen.FullRowSelect = true;
            this.lvDomen.GridLines = true;
            this.lvDomen.Location = new System.Drawing.Point(6, 16);
            this.lvDomen.Name = "lvDomen";
            this.lvDomen.Size = new System.Drawing.Size(343, 355);
            this.lvDomen.TabIndex = 0;
            this.lvDomen.UseCompatibleStateImageBehavior = false;
            this.lvDomen.View = System.Windows.Forms.View.Details;
            this.lvDomen.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvDomen_ItemDrag);
            this.lvDomen.SelectedIndexChanged += new System.EventHandler(this.lvDomen_SelectedIndexChanged);
            this.lvDomen.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvDomen_DragDrop);
            this.lvDomen.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvDomen_DragEnter);
            this.lvDomen.DragOver += new System.Windows.Forms.DragEventHandler(this.lvDomen_DragOver);
            this.lvDomen.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvDomen_MouseDoubleClick);
            // 
            // domenIndex
            // 
            this.domenIndex.Text = "№";
            this.domenIndex.Width = 37;
            // 
            // domenName
            // 
            this.domenName.Text = "Название";
            this.domenName.Width = 292;
            // 
            // gbActions
            // 
            this.gbActions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbActions.Controls.Add(this.btnDeleteDomen);
            this.gbActions.Controls.Add(this.btnEditDomen);
            this.gbActions.Controls.Add(this.btnAddDomen);
            this.gbActions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbActions.Location = new System.Drawing.Point(372, 12);
            this.gbActions.Name = "gbActions";
            this.gbActions.Size = new System.Drawing.Size(284, 122);
            this.gbActions.TabIndex = 1;
            this.gbActions.TabStop = false;
            this.gbActions.Text = "Действия";
            // 
            // btnDeleteDomen
            // 
            this.btnDeleteDomen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteDomen.Location = new System.Drawing.Point(11, 83);
            this.btnDeleteDomen.Name = "btnDeleteDomen";
            this.btnDeleteDomen.Size = new System.Drawing.Size(267, 26);
            this.btnDeleteDomen.TabIndex = 2;
            this.btnDeleteDomen.Text = "Удалить";
            this.btnDeleteDomen.UseVisualStyleBackColor = true;
            this.btnDeleteDomen.Click += new System.EventHandler(this.btnDeleteDomen_Click);
            // 
            // btnEditDomen
            // 
            this.btnEditDomen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditDomen.Location = new System.Drawing.Point(11, 51);
            this.btnEditDomen.Name = "btnEditDomen";
            this.btnEditDomen.Size = new System.Drawing.Size(267, 26);
            this.btnEditDomen.TabIndex = 1;
            this.btnEditDomen.Text = "Редактировать";
            this.btnEditDomen.UseVisualStyleBackColor = true;
            this.btnEditDomen.Click += new System.EventHandler(this.btnEditDomen_Click);
            // 
            // btnAddDomen
            // 
            this.btnAddDomen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddDomen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddDomen.Location = new System.Drawing.Point(11, 19);
            this.btnAddDomen.Name = "btnAddDomen";
            this.btnAddDomen.Size = new System.Drawing.Size(267, 26);
            this.btnAddDomen.TabIndex = 0;
            this.btnAddDomen.Text = "Добавить";
            this.btnAddDomen.UseVisualStyleBackColor = true;
            this.btnAddDomen.Click += new System.EventHandler(this.btnAddDomen_Click);
            // 
            // gbCurrentDomen
            // 
            this.gbCurrentDomen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCurrentDomen.Controls.Add(this.lvCurrentDomen);
            this.gbCurrentDomen.Location = new System.Drawing.Point(372, 140);
            this.gbCurrentDomen.Name = "gbCurrentDomen";
            this.gbCurrentDomen.Size = new System.Drawing.Size(284, 193);
            this.gbCurrentDomen.TabIndex = 2;
            this.gbCurrentDomen.TabStop = false;
            this.gbCurrentDomen.Text = "Текущий домен";
            // 
            // lvCurrentDomen
            // 
            this.lvCurrentDomen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lvCurrentDomen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.domenValueIndex,
            this.domenValueName});
            this.lvCurrentDomen.FullRowSelect = true;
            this.lvCurrentDomen.GridLines = true;
            this.lvCurrentDomen.Location = new System.Drawing.Point(11, 19);
            this.lvCurrentDomen.Name = "lvCurrentDomen";
            this.lvCurrentDomen.Size = new System.Drawing.Size(267, 168);
            this.lvCurrentDomen.TabIndex = 0;
            this.lvCurrentDomen.UseCompatibleStateImageBehavior = false;
            this.lvCurrentDomen.View = System.Windows.Forms.View.Details;
            this.lvCurrentDomen.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvCurrentDomen_ItemDrag);
            this.lvCurrentDomen.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvCurrentDomen_DragDrop);
            this.lvCurrentDomen.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvCurrentDomen_DragEnter);
            this.lvCurrentDomen.DragOver += new System.Windows.Forms.DragEventHandler(this.lvCurrentDomen_DragOver);
            // 
            // domenValueIndex
            // 
            this.domenValueIndex.Text = "№";
            this.domenValueIndex.Width = 30;
            // 
            // domenValueName
            // 
            this.domenValueName.Text = "Название";
            this.domenValueName.Width = 226;
            // 
            // btnSaveAndClose
            // 
            this.btnSaveAndClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAndClose.Location = new System.Drawing.Point(372, 339);
            this.btnSaveAndClose.Name = "btnSaveAndClose";
            this.btnSaveAndClose.Size = new System.Drawing.Size(284, 23);
            this.btnSaveAndClose.TabIndex = 3;
            this.btnSaveAndClose.Text = "Сохранить и закрыть";
            this.btnSaveAndClose.UseVisualStyleBackColor = true;
            this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(372, 372);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(284, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Закрыть";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // DomenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 407);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSaveAndClose);
            this.Controls.Add(this.gbCurrentDomen);
            this.Controls.Add(this.gbActions);
            this.Controls.Add(this.gbDomen);
            this.Name = "DomenForm";
            this.Text = "Домены";
            this.gbDomen.ResumeLayout(false);
            this.gbActions.ResumeLayout(false);
            this.gbCurrentDomen.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDomen;
        private System.Windows.Forms.ListView lvDomen;
        private System.Windows.Forms.ColumnHeader domenName;
        private System.Windows.Forms.ColumnHeader domenIndex;
        private System.Windows.Forms.GroupBox gbActions;
        private System.Windows.Forms.Button btnDeleteDomen;
        private System.Windows.Forms.Button btnEditDomen;
        private System.Windows.Forms.Button btnAddDomen;
        private System.Windows.Forms.GroupBox gbCurrentDomen;
        private System.Windows.Forms.ListView lvCurrentDomen;
        private System.Windows.Forms.ColumnHeader domenValueName;
        private System.Windows.Forms.Button btnSaveAndClose;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ColumnHeader domenValueIndex;
    }
}