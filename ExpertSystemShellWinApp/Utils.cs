﻿namespace ExpertSystemShellWinApp
{
    using System.Collections.Generic;
    using System.Windows.Forms;

    using ExpertSystemShellWinApp.Entities;

    public class Utils
    {
        #region Для Drag and Drop.
        public static void DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        public static void DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListView.SelectedListViewItemCollection)))
                e.Effect = e.AllowedEffect;
        }

        public static void ItemDrag(Control control, object sender, ItemDragEventArgs e)
        {
            control.DoDragDrop(e.Item, DragDropEffects.Copy);
        }
        #endregion

        // Словарь тип переменной-название
        public static Dictionary<EVariableType, string> DictVarialbeType = new Dictionary<EVariableType, string>()
        {
            {EVariableType.Requested, "Запрашиваемая" },
            {EVariableType.Derivable, "Выводимая" },
            {EVariableType.DerivableRequested, "Выводимо-запрашиваемая" }
        };

        public static void ShowError(string typeName)
        {
            MessageBox.Show($"{typeName} с таким именем уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
