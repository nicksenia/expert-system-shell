﻿namespace ExpertSystemShellWinApp.Entities
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class Rule : IEntity
    {
        public string Name { get; set; }
        public List<Fact> Premises { get; set; }
        public List<Fact> Consequences { get; set; }
        public string Reason { get; set; }

        public Rule(string name, List<Fact> premises, List<Fact> consequences, Guid uuid, int order) : base(uuid, order)
        {
            Name = name.Trim();
            Premises = premises;
            Consequences = consequences;
        }

        public Rule(Rule oldRule) : base(oldRule.Uuid, oldRule.Order)
        {
            Name = oldRule.Name;
            Premises = oldRule.Premises;
            Consequences = oldRule.Consequences;
            Reason = oldRule.Reason;
        }

        public Rule() { }

        public override string ToString()
        {
            return $"ЕСЛИ {string.Join(" И ", Premises)} ТО {string.Join(" И ", Consequences)}";
        }
    }
}
