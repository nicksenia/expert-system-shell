﻿namespace ExpertSystemShellWinApp.Entities
{
    using System;

    [Serializable]
    public class DomenVal : IEntity
    {
        public string Value { get; set; }

        public DomenVal() { }

        public DomenVal(string val, Guid uuid, int order) : base (uuid, order)
        {
            Value = val;
        }
    }
}
