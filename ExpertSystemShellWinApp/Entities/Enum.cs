﻿namespace ExpertSystemShellWinApp.Entities
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public enum EVariableType
    {
        [XmlEnum(Name = "Requested")]
        Requested,
        [XmlEnum(Name = "Derivable")]
        Derivable,
        [XmlEnum(Name = "DerivableRequested")]
        DerivableRequested
    }
}
