﻿namespace ExpertSystemShellWinApp.Entities
{
    using System;

    [Serializable]
    public class Variable : IEntity
    {
        public string Question { get; set; }
        public string Name { get; set; }
        public Domen Domen { get; set; }
        public EVariableType VariableType { get; set; }

        public Variable(string name, Domen domen, EVariableType variableType, string question, Guid uuid, int order) : base(uuid, order)
        {
            Question = question;
            Name = name.Trim();
            Domen = domen;
            VariableType = variableType;
        }

        public Variable(Variable oldVariable) : base(oldVariable.Uuid, oldVariable.Order)
        {
            Question = oldVariable.Question;
            Name = oldVariable.Name;
            Domen = new Domen(oldVariable.Domen);
            VariableType = oldVariable.VariableType;
        }

        public Variable() { }
    }
}
