﻿namespace ExpertSystemShellWinApp.Entities
{
    using System;

    [Serializable]
    public abstract class IEntity
    {
        public Guid Uuid { get; set; }
        public Int32 Order { get; set; }

        public IEntity(Guid uuid, Int32 order)
        {
            Uuid = uuid;
            Order = order;
        }

        public IEntity() { }
    }
}
