﻿namespace ExpertSystemShellWinApp.Entities
{
    using System;

    [Serializable]
    public class Fact : IEntity
    {
        public DomenVal Value { get; set; }
        public Variable Variable { get; set; }

        public Fact(DomenVal domenVal, Variable variable, Guid uuid, int order) : base(uuid, order)
        {
            Value = domenVal;
            Variable = variable;
        }

        public Fact(Fact oldFact) : base(oldFact.Uuid, oldFact.Order)
        {
            Value = oldFact.Value;
            Variable = new Variable(oldFact.Variable);
        }

        public override string ToString()
        {
            return $"{Variable.Name} = {Value.Value}";
        }

        public Fact() { }
    }
}
