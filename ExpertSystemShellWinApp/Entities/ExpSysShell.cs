﻿namespace ExpertSystemShellWinApp.Entities
{
    using ExpertSystemShellWinApp.Forms;

    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    public class ExpSysShell
    {
        // Цепочка сработавших правил.
        public Dictionary<Rule, int> ChainUsedRules { get; }

        // База правил.
        public BaseRule BaseRule { get; set; }

        // Текущая цель консультации.
        public Variable Goal { get; set; }

        // Продолжать консультацию.
        public bool IsContinue { get; set; }

        // Форма для вопросов.
        private ConsultForm ConsultForm;

        // Рабочая память.
        public Dictionary<string, DomenVal> WorkMemory;

        // Список использованных переменных.
        private Dictionary<string, bool> UsedVariables = new Dictionary<string, bool>();

        // Список использованных правил.
        private List<Rule> UsedRules = new List<Rule>();

        // Последняя использованная переменная.
        private string lastUsedVariable = string.Empty;

        public ExpSysShell()
        {
            ChainUsedRules = new Dictionary<Rule, int>();
            BaseRule = new BaseRule();
            IsContinue = true;
            WorkMemory = new Dictionary<string, DomenVal>();
            Goal = new Variable();
        }

        // Консультация.
        public void Consult()
        {
            var current = Goal;
            ConsultForm = new ConsultForm(current.Name);
            WorkMemory.Clear();
            UsedRules.Clear();
            UsedVariables.Clear();
            ChainUsedRules.Clear();
            foreach (var variable in BaseRule.VariableSet)
            {
                UsedVariables.Add(variable.Name, false);
            }

            IsContinue = true;
            if (Work(current, 0))
            {
                if (WorkMemory.ContainsKey(current.Name))
                {
                    MessageBox.Show($"Цель консультации определилась! Результат - {Goal.Name} = {WorkMemory[current.Name].Value}", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show($"Цель консультации не определилась!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            ConsultForm.Dispose();
        }

        // Работа консультации.
        private bool Work(Variable current, int number)
        {
            if (IsContinue)
            {
                if (current.VariableType == EVariableType.Requested && AskQuestion(current) != DialogResult.OK)
                {
                    return false;
                }

                foreach (var rule in BaseRule.RuleSet.OrderBy(x => x.Order))
                {
                    if (UsedRules.Contains(rule) || !rule.Consequences.Where(x => x.Variable.VariableType != EVariableType.Requested).Any(x => x.Variable.Name == current.Name))
                    {
                        continue;
                    }
                    else
                    {
                        UsedRules.Add(rule);
                        // Обработка посылки.
                        var flag = true;
                        foreach (var fact in rule.Premises)
                        {
                            if (!UsedVariables[fact.Variable.Name] && !Work(fact.Variable, number + 1))
                            {
                                return false;
                            }
                            // Если переменная еще не означена или значение в памяти не совпадает с тем, что в поссылке.
                            if (!UsedVariables[fact.Variable.Name] || WorkMemory[fact.Variable.Name].Value != fact.Value.Value)
                            {
                                flag = false;
                                break;
                            }
                        }
                        if (flag)
                        {
                            foreach (var fact in rule.Consequences)
                            {
                                UsedVariables[fact.Variable.Name] = true;
                                WorkMemory[fact.Variable.Name] = fact.Value;
                            }
                            ChainUsedRules.Add(rule, number);
                        }
                    }
                }
                if (current.VariableType == EVariableType.DerivableRequested)
                {
                    // Есть вероятность, что она уже означена ранее.
                    if (!WorkMemory.ContainsKey(current.Name) || !UsedVariables[current.Name])
                    {
                        AskQuestion(current);
                    }
                }
                return true;
            }
            return false;
        }

        // Логика вопросов.
        private DialogResult AskQuestion(Variable variable)
        {
            // Вызывается вопросная форма.
            ConsultForm.AskQuestion(variable);
            ConsultForm.StartPosition = FormStartPosition.CenterScreen;
            var result = ConsultForm.ShowDialog();
            // Если нажали ОК, то обычная обработка.
            if (result == DialogResult.OK)
            {
                UsedVariables[variable.Name] = true;
                WorkMemory[variable.Name] = ConsultForm.Answer;
            }
            // Если нажали вернуться к предыдущему.
            if (result == DialogResult.Cancel)
            {
                UsedVariables[lastUsedVariable] = false;
                WorkMemory.Remove(lastUsedVariable);
            }
            // Если нажали закончить консультацию.
            if (result == DialogResult.Abort)
            {
                IsContinue = false;
            }

            return result;
        }
    }
}
