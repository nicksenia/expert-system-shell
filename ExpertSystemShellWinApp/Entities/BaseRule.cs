﻿namespace ExpertSystemShellWinApp.Entities
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Serialization;

    [Serializable]
    public class BaseRule
    {
        public string Name { get; set; }
        public List<Domen> DomenSet { get; set; }
        public List<Variable> VariableSet { get; set; }
        public List<Rule> RuleSet { get; set; }

        public BaseRule()
        {
            DomenSet = new List<Domen>();
            VariableSet = new List<Variable>();
            RuleSet = new List<Rule>();
        }

        public void Save(string fileName)
        {
            var formatter = new XmlSerializer(typeof(BaseRule),
                new Type[]{ typeof(BaseRule), typeof(Domen), typeof(DomenVal), typeof(EVariableType),
                            typeof(Fact), typeof(Rule), typeof(Variable)});
            using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, this);
            }
        }

        public BaseRule Load(string fileName)
        {
            var formatter = new XmlSerializer(typeof(BaseRule),
                new Type[]{ typeof(BaseRule), typeof(Domen), typeof(DomenVal), typeof(EVariableType),
                            typeof(Fact), typeof(Rule), typeof(Variable)});
            using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                BaseRule baserule = (BaseRule)formatter.Deserialize(fs);
                return baserule;
            }
        }
    }
}
