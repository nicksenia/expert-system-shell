﻿namespace ExpertSystemShellWinApp.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [Serializable]
    public class Domen : IEntity
    {
        public string Name { get; set; }
        public List<DomenVal> DomenValues { get; set; }

        public Domen(string name, List<DomenVal> domenValues, Guid uuid, int order) : base(uuid, order)
        {
            Name = name;
            DomenValues = domenValues;
        }

        public Domen(Domen oldDomen) : base(oldDomen.Uuid, oldDomen.Order)
        {
            Name = oldDomen.Name;
            DomenValues = oldDomen.DomenValues.Select(x => new DomenVal(x.Value, x.Uuid, x.Order)).ToList();
        }

        public Domen() { }
    }
}
